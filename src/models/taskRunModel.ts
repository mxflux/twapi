/* eslint-disable no-unused-vars */
import mongoose, { Document, Schema } from 'mongoose';

export enum TaskRunStatus {
    success = 'success',
    failure = 'failure',
    running = 'running',
    cancelled = 'cancelled'
}

export interface ITaskRun extends Document {
    task: string;
    status: TaskRunStatus;
    result: any;
    error: any;
    createdAt: Date;
    updatedAt: Date;
    skipCount: number;
}

const TaskRunSchema = new Schema({
  task: {
    type: Schema.Types.ObjectId,
    ref: 'Task'
  },
  status: {
    type: String,
    enum: Object.keys(TaskRunStatus),
    default: TaskRunStatus.running
  },
  result: Schema.Types.Mixed,
  error: Schema.Types.Mixed,
  skipCount: { type: Schema.Types.Number, default: 0 }
}, { timestamps: true });

export default mongoose.model<ITaskRun>('TaskRun', TaskRunSchema);

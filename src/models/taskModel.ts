/* eslint-disable camelcase */
import mongoose, { Schema, model } from 'mongoose';

export interface Task extends mongoose.Document {
    name: string,
    active: boolean,
    service_path: string,
    cron_rule?: string,
    last_run?: Date,
    options?: object,
    maxSkipCount?: number
}

const TaskSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Task Name ist required'],
    unique: true
  },
  active: { type: Boolean, default: false },
  cron_rule: {
    type: String
  },
  service_path: {
    type: String,
    required: [true, 'Path to Service file is required']
  },
  last_run: Date,
  options: Object,
  maxSkipCount: { type: Number, default: 0 }
});

export default model<Task>('Task', TaskSchema);

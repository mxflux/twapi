import * as rp from 'request-promise';

interface Header {
    'content-type': string,
    authorization?: string,
}

interface Option {
    json: string,
    headers: Header,
    method?: string,
    uri: string,
    body?: any,
}

class TwAbstractApiClient {
    private uri: string;

    private keyId: string | null;

    private keySecret: string | null;

    constructor (uri: string, keyId: string | null = null, keySecret: string | null = null) {
      if (new.target === TwAbstractApiClient) {
        throw new TypeError('Cannot construct Abstract instances directly');
      }
      this.uri = uri;
      this.keyId = keyId;
      this.keySecret = keySecret;
    }

    list (query: any | null = null, sorting: string | null = null,
      paging: string | null = null,
      projection: string | null = null): Promise<any[]> {
      let queryPath = '';
      if (query || sorting || paging) {
        queryPath += '?';
        const params = [];
        if (Object.keys(query).length) {
          params.push('q=' + encodeURIComponent(JSON.stringify(query)));
        }
        if (sorting) {
          params.push('s=' + encodeURIComponent(JSON.stringify(sorting)));
        }
        if (paging) {
          params.push('p=' + encodeURIComponent(JSON.stringify(paging)));
        }
        if (projection) {
          params.push('o=' + encodeURIComponent(JSON.stringify(projection)));
        }
        queryPath += params.join('&');
      }

      return new Promise((resolve, reject) => {
        const options = this.getOptions();
        options.uri += queryPath;
        rp.get(options).then((res: any) => {
          resolve(res);
        }).catch((err: any) => {
          reject(err);
        });
      });
    }

    get (id: string): Promise<any> {
      return new Promise((resolve, reject) => {
        const options = this.getOptions();
        options.uri += id;
        rp.get(options).then((res: any) => {
          resolve(res);
        }).catch((err: any) => {
          reject(err);
        });
      });
    }

    post (data: any): Promise<any> {
      return new Promise((resolve, reject) => {
        const options = this.getOptions();
        options.method = 'POST';
        options.body = data;
        rp.post(options).then((res: any) => {
          resolve(res);
        }).catch((err: any) => {
          reject(err);
        });
      });
    }

    put (id: string, data: any): Promise<any> {
      return new Promise((resolve, reject) => {
        const options = this.getOptions();
        options.method = 'PUT';
        options.body = data;
        options.uri += id;
        rp.put(options).then((res: any) => {
          resolve(res);
        }).catch((err: any) => {
          reject(err);
        });
      });
    }

    getOptions (): Option {
      const options: any = {
        json: true,
        uri: this.getUir(),
        headers: {
          'content-type': 'application/json'
        }
      };
      if (this.keyId && this.keySecret) {
        options.headers.authorization = JSON.stringify({
          KEY_ID: this.keyId,
          KEY_SECRET: this.keySecret
        });
      }
      return options as Option;
    }

    getUir () {
      return this.uri.replace(/\/?$/, '/');
    }
}

export default TwAbstractApiClient;

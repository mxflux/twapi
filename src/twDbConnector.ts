import mongoose from 'mongoose';

class TwDbConnector {
    private host: string;

    private database: string;

    private connected: boolean;

    constructor (host: string, database: string) {
      this.host = host;
      this.database = database;
      mongoose.Promise = global.Promise;
    }

    connect () {
      if (!this.connected) {
        this.connected = true;
        mongoose.connect(this.host, {
          dbName: this.database
        }).then(() => {
          console.log('Connected to MongoDB ' + this.host + '/' + this.database);
        }).catch((err) => {
          console.error(err.toString());
          this.connected = false;
        });
      }
    }

    disconnect () {
      return new Promise<void>(
        (resolve, reject) => {
          this.connected = false;
          mongoose.disconnect().then(() => {
            console.log(`MongoDB ${this.host} ${this.database} disconnected`);
            resolve();
          }).catch((err) => {
            console.error(err.toString());
            reject(err);
          });
        }
      );
    }
}

export default TwDbConnector;

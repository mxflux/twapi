/* eslint-disable camelcase */
import { Application } from 'express';
import TwAbstractApiClient from './twAbstractApiClient';

const si = require('systeminformation');

class TwWatchPingApi extends TwAbstractApiClient {
  constructor (uri: string, keyId: string, keySecret: string) {
    super(uri + 'pings/', keyId, keySecret);
  }
}

class TwPing {
    private app: Application;

    private status: string;

    private system_info: string;

    constructor (input: any) {
      this.app = input.app;
      this.status = input.status;
      this.system_info = input.system_info;
    }
}

class TwWatchService {
    private config: any;

    private status: string;

    public pingApi: TwWatchPingApi;

    private interval: any;

    constructor (config: any) {
      this.config = config;
      this.status = 'normal';
      this.pingApi = new TwWatchPingApi(this.config.url, this.config.key_id, this.config.key_secret);

      const interval = (this.config.interval) ? this.config.interval : 60;
      this.interval = setInterval(this.ping.bind(this), (interval * 1000));
      console.log('TwWatchService: Started');
    }

    async ping () {
      let pjson = 'unknown';
      try {
        pjson = require.main.require('../package.json');
      } catch (e) {
        console.error('TwWatchService: Could not find package.json');
      }
      const system_info: any = {};
      system_info.cpu = await si.currentLoad();
      system_info.memory = await si.mem();
      const ping = new TwPing({
        app: (pjson as any).name,
        status: this.status,
        system_info: system_info
      });
      await this.pingApi.post(ping).catch(() => {
        console.log('TwWatchService: Error while sending ping to tw watch server.');
      });
    }

    disable () {
      console.log('TwWatchService: Stopped');
      clearInterval(this.interval);
    }
}

export default TwWatchService;

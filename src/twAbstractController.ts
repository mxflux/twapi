import { Request, Response } from 'express';
import { Document, Error, Model } from 'mongoose';
import { Indexable } from './types/basicTypes';

interface POPOBJ {
    populate: any[],
}

interface Config {
    list?: POPOBJ,
    get?: POPOBJ,
}

class TwAbstractController {
    protected config: Config;

    protected model: Model<Document>;

    constructor (model: Model<any> | null = null) {
      this.config = {
        list: { populate: [] },
        get: { populate: [] }
      };
      if (new.target === TwAbstractController) {
        throw new TypeError('Cannot construct Abstract instances directly');
      }
      this.model = model;
    }

    /**
     * expects no parameters but a query eg:
     * http://localhost:3000/surveys?q={"zahl": {"$gt":1}}&s={"zahl":1}&p={"limit":2,"offset":1}&o={"subItems.$":1}
     * q = Query
     * s = Sorting
     * p = Paging (limit, offset)
     * o = Projection
     * @param req
     * @param res
     */
    async list (req: Request, res: Response):Promise<any> {
      let queryFields;
      try {
        queryFields = this.getQuery(req);
      } catch (e) {
        return res.status(400).json(e);
      }
      return this.createList(res, req, queryFields);
    }

    createList (res: Response, req: Request, queryFields: any) {
      let query: any = this.model.find(queryFields.query, (err: Error, doc: Document) => {
        if (err) {
          return res.status(400).send(err);
        }
        // get total count
        this.model.countDocuments(queryFields.query, (err2: Error, count: number) => {
          if (!err2) {
            res.header('Access-Control-Expose-Headers', 'X-Total-Count');
            res.set('X-Total-Count', String(count));
            this.createPagingLinks(req, res, queryFields.limit, queryFields.offset, count);
          }
          res.json(doc);
        });
      });
      if (this.config.list.populate.length > 0) {
        this.config.list.populate.forEach((popField: any) => {
          query = query.populate(popField);
        });
      }
      if (queryFields.populate.length > 0) {
        queryFields.populate.forEach((popField: any) => {
          query = query.populate(popField);
        });
      }
      query.sort(queryFields.sorting).skip(queryFields.offset).limit(queryFields.limit);
      if (queryFields.projection) {
        query.select(queryFields.projection);
      }
    }

    getQuery (req: Request) {
      let query: Indexable = {};
      let sorting: Indexable = {};
      let limit = 0;
      let offset = 0;
      let projection;
      const populate = [];
      // check for  q(query),s (sorting),p (paging) parameter
      if (req.query) {
        if (req.query.o) {
          try {
            projection = JSON.parse(decodeURIComponent(req.query.o as string));
          } catch (e) {
            throw new Error('Could not parse Query String');
          }
        }
        if (req.query.q) {
          try {
            query = JSON.parse(decodeURIComponent(req.query.q as string));
            for (const key of Object.keys(query)) {
              if (key.indexOf('.') !== -1) {
                const rootPath = key.split('.').splice(0, 1).join('.');
                // TO-DO: Think about: Is the path function applicable here???
                if (((this.model.schema as any).paths as Indexable)[rootPath] &&
                                    (((this.model.schema as any).paths as Indexable)[rootPath] as any).instance === 'ObjectID') {
                  const match: Indexable = {};
                  match[key.replace(rootPath + '.', '')] = query[key];
                  populate.push({ path: rootPath, match: match });
                  delete query[key];
                }
              }
            }
          } catch (e) {
            throw new Error('Could not parse Query String');
          }
        }
        if (req.query.s) {
          try {
            sorting = JSON.parse(decodeURIComponent(req.query.s as string)) as Indexable;
            for (const field in sorting) {
              if (field) {
                if (sorting[field] === 'desc') {
                  sorting[field] = -1;
                }
                if (sorting[field] === 'asc') {
                  sorting[field] = 1;
                }
              }
            }
          } catch (e) {
            throw new Error('Could not parse Sorting String');
          }
        }
        if (req.query.p) {
          try {
            const p = JSON.parse(decodeURIComponent(req.query.p as string));
            if (p.limit) {
              if (p.limit !== parseInt(p.limit, 10)) {
                throw new Error('Limit must be an Integer');
              }
              limit = p.limit;
            }
            if (p.offset) {
              if (p.offset !== parseInt(p.offset, 10)) {
                throw new Error('Offset must be an Integer');
              }
              offset = p.offset;
            }
          } catch (e) {
            throw new Error('Could not parse Paging String\n ' + e.toString());
          }
        }
      }
      return {
        query: query,
        sorting: sorting,
        limit: limit,
        offset: offset,
        projection: projection,
        populate: populate
      };
    }

    async get (req: Request, res: Response) {
      const id = req.params.id;
      try {
        let query = this.getItem(id);
        if (this.config.get.populate.length > 0) {
          this.config.get.populate.forEach((popField: any) => {
            query = query.populate(popField);
          });
        }
        query.then((document: any) => {
          if (document) {
            res.status(200).json(document);
          } else {
            res.status(404).send();
          }
        }).catch(() => {
          res.status(404).send();
        });
      } catch (e) {
        res.status(404).send();
      }
    }

    async post (req: Request, res: Response) {
      // eslint-disable-next-line new-cap
      const model = new this.model(req.body);
      this.saveModel(model).then((result) => {
        res.json(result);
      }).catch(err => {
        res.status(400).json(err);
      });
    }

    async put (req: Request, res: Response) {
      const id = req.params.id;
      try {
        this.getItem(id).then((document: any) => {
          document.set(req.body);
          this.saveDocument(document).then(result => {
            res.json(result);
          }).catch(err => {
            res.status(400).json(err);
          });
        }).catch(() => {
          res.status(404).send();
        });
      } catch (e) {
        res.status(404).send();
      }
    }

    async patch (req: Request, res: Response) {
      const id = req.params.id;
      try {
        this.getItem(id).then((model: any) => {
          model.set(req.body);
          this.saveModel(model).then((result) => {
            res.json(result);
          }).catch((err) => {
            res.status(400).json(err);
          });
        }).catch(() => {
          res.status(404).send();
        });
      } catch (e) {
        res.status(404).send();
      }
    }

    async delete (req: Request, res: Response) {
      const id = req.params.id;
      try {
        this.getItem(id).then((document: any) => {
          document.remove().then((result: any) => {
            res.json(result);
          }).catch((err: any) => {
            res.status(500).json(err);
          });
        }).catch((err: any) => {
          res.status(500).json(err);
        });
      } catch (e) {
        res.status(404).send();
      }
    }

    createPagingLinks (req: Request, res: Response, limit: number, offset: number, count: number) {
      if (req.query.p) {
        let queryString = '';
        for (const name in req.query) {
          if (name !== 'p') {
            queryString += '&' + name + '=' + req.query[name];
          }
        }
        queryString = queryString.substr(1);
        const lastOffset = Math.floor(count / limit) * limit;
        const lastLimit = (count - lastOffset);
        const nextLimit = (lastOffset === offset + 1 * limit) ? lastLimit : limit;
        if (lastOffset !== offset) {
          res.append('Link', [`<${req.baseUrl}?${queryString}&p={"limit":${nextLimit},"offset":${offset + 1 * limit}>; rel="next"`]);
        }
        if (offset !== 0) {
          res.append('Link', [`<${req.baseUrl}?${queryString}&p={"limit":${limit},"offset":${offset - 1 * limit}>; rel="prev"`]);
        }
        res.append('Link', [`<${req.baseUrl}?${queryString}&p={"limit":${limit},"offset":0>; rel="first"`]);
        res.append('Link', [`<${req.baseUrl}?${queryString}&p={"limit":${lastLimit},"offset":${lastOffset}>; rel="last"`]);
      }
    }

    saveDocument (document: Document) {
      return new Promise((resolve, reject) => {
        document.save().then(result => {
          resolve(result);
        }).catch(err => {
          if (err.name === 'MongoError' && err.code === 11000) {
            err = { error: { message: err.errmsg } };
          }
          reject(err);
        });
      });
    }

    getItem (id: string): any {
      return this.model.findById(id);
    }

    // !!! deprecated !!!
    saveModel (model: any) {
      return new Promise((resolve, reject) => {
        model.save().then((result: any) => {
          resolve(result);
        }).catch((err: any) => {
          if (err.name === 'MongoError' && err.code === 11000) {
            err = { error: { message: err.errmsg } };
          }
          reject(err);
        });
      });
    }
}

export default TwAbstractController;

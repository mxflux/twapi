import express, { Application } from 'express';
import TwDbConnector from './twDbConnector';
import TwWatchService from './twWatchService';
import twTaskService from './twTaskService';
import { Server } from 'http';
import serverlessExpress from 'aws-serverless-express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

class TwApp {
    public app: Application | any;

    public twDbConnector: TwDbConnector;

    private twWatchService: TwWatchService;
    private server: Server;

    constructor (public port = 3000, public isInLambda = false) {
      this.app = express();
      this.app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
      this.app.use(bodyParser.json({ limit: '50mb' }));
      this.app.use(cookieParser());
      this.isInLambda = isInLambda;
    }

    connectDB (host: string, database: string) {
      this.twDbConnector = new TwDbConnector(host, database);
      this.twDbConnector.connect();
    }

    enableWatchSerive (watchServiceConfig: any) {
      if (!this.isInLambda) {
        this.twWatchService = new TwWatchService(watchServiceConfig);
      }
    }

    registerTask (task: any) {
      twTaskService.registerTask(task);
    }

    enableTasks () {
      if (!this.isInLambda) {
        twTaskService.enable();
      }
    }

    addRoutes (routes: any) {
      routes(this.app);
    }

    setPort (port: number) {
      this.port = port;
    }

    listen () {
      if (this.isInLambda) {
        return this.listenLambda();
      } else {
        if (!this.server || !this.server.listening) {
          this.server = this.app.listen(this.port);
          console.log('TwApp: RESTful API server started on: ' + this.port + ' with PID ' + process.pid);
        }
        return this.app;
      }
    }

    listenLambda () {
      const server = serverlessExpress.createServer(this.app);
      // eslint-disable-next-line no-return-assign
      return this.app = exports.lambdaHandler = (event: any, context: any) => {
        context.callbackWaitsForEmptyEventLoop = false;
        console.log('TwApp: Lambda Server');
        // fix issue with base path url
        if (event.pathParameters && event.pathParameters.proxy) {
          let newPath = '/';
          // add stage if previously included in path
          if (event.path.includes(event.requestContext.stage)) {
            newPath += event.requestContext.stage + '/';
          }
          newPath += event.pathParameters.proxy;
          event.path = newPath;
        }
        if (event.service) {
          return this.executeTask(event);
        } else {
          return serverlessExpress.proxy(server, event, context);
        }
      };
    }

    async executeTask (event: any) {
      try {
        console.log('lambda task started');
        const task = await twTaskService.getTaskByName(event.service);
        const options = (event.options) ? event.options : task.options;
        await twTaskService.executeTask(task, options);
        console.log('lambda task ended');
      } catch (e) {
        console.error(e);
        throw e;
      }
    }

    async close () {
      if (this.server) {
        await this.server.close();
      }
      console.log('closed server');
      if (this.twDbConnector) {
        await this.twDbConnector.disconnect();
      }
      console.log('closed db');
      if (this.twWatchService) {
        await this.twWatchService.disable();
      }
      await twTaskService.disable();
      console.log('ended');
    }
}

export default TwApp;

import DummyFactory from './lib/DummyFactory';
import twApp from './lib/app';
import itemModel from './lib/itemModel';

import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

const dummyFactory = new DummyFactory(itemModel);

describe('Abstract Router Get', () => {
  before((done) => {
    twApp.listen();
    twApp.twDbConnector.connect();
    itemModel.deleteMany({}, () => {
      done();
    });
  });
  after((done) => {
    twApp.close().then(() => done());
  });
  beforeEach((done) => {
    dummyFactory.generate(10, done);
  });
  afterEach((done) => {
    itemModel.deleteMany({}, () => {
      done();
    });
  });

  describe('/GET', () => {
    it('it should GET no items', done => {
      chai.request(twApp.app)
        .get('/items/fakeid')
        .end((_err, res) => {
          res.should.have.status(404);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });

    it('it should return item', (done) => {
      const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
      chai.request(twApp.app)
        .get('/items/' + randomItem._id)
        .end((_err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name');
          res.body.should.have.property('number');
          res.body.name.should.be.equal(randomItem.name);
          res.body.number.should.be.equal(randomItem.number);
          done();
        });
    });
  });
});

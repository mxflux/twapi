import mongoose from 'mongoose';
import DummyFactory from './lib/DummyFactory';
import twApp from './lib/app';
import ItemModel from './lib/itemModel';

import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

chai.use(chaiHttp);
chai.use(spies);

process.env.NODE_ENV = 'test';

// mongoose.models = {};
(mongoose as any).modelSchemas = {};
const app = twApp.app;
chai.use(require('chai-http'));

const dummyFactory = new DummyFactory(ItemModel);

describe('Abstract Router Create', () => {
  before((done) => {
    twApp.listen();
    twApp.twDbConnector.connect();
    ItemModel.deleteMany({}, () => {
      done();
    });
  });
  after((done) => {
    twApp.close().then(() => done());
  });
  afterEach((done) => {
    ItemModel.deleteMany({}, () => {
      done();
    });
  });

  describe('/POST', () => {
    it('it should not create item without number', (done) => {
      const item = { name: dummyFactory.makeString() };
      chai.request(app)
        .post('/items')
        .send(item)
        .end((_err: any, res: any) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.number.should.have.property('kind').eql('required');
          done();
        });
    });

    it('it should not create item with same number', (done) => {
      const item = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber() };
      const mItem = new ItemModel(item as any);
      mItem.save().then((mItem: any) => {
        chai.request(app)
          .post('/items')
          .send(item)
          .end((_err: any, res: any) => {
            res.should.have.status(400);
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            res.body.error.should.have.property('message');
            done();
          });
      });
    });

    it('it should create item', (done) => {
      const item = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber() };
      chai.request(app)
        .post('/items')
        .send(item)
        .end((_err: any, res: any) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name');
          res.body.should.have.property('number');
          res.body.should.have.property('_id');
          res.body.name.should.be.equal(item.name);
          res.body.number.should.be.equal(item.number);
          done();
        });
    });
  });
});

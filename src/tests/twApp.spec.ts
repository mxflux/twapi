import TwApp from '../twApp';
import config from './lib/config';
import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

describe('TwApp', () => {
  it('it should create new TwApp with default port', () => {
    const twApp = new TwApp();
    twApp.port.should.be.equal(3000);
  });

  it('it should create new TwApp with custom port', () => {
    const twApp = new TwApp(config.PORT + 1);
    twApp.port.should.be.equal(config.PORT + 1);
  });

  it('it should set a new port', () => {
    const twApp = new TwApp();
    twApp.setPort(2000);
    twApp.port.should.be.equal(2000);
  });

  it('it should create new TwApp with lambda environment', () => {
    const twApp = new TwApp(config.PORT, true);
    twApp.port.should.be.equal(config.PORT);
    twApp.isInLambda.should.be.equal(true);
  });

  it('it should listen as lamba server', () => {
    const twApp = new TwApp(config.PORT, true);
    const spy = chai.spy.on(twApp, 'listenLambda');
    twApp.listen();
    spy.should.have.been.called();
  });

  it('it should listen as express server', () => {
    const twApp = new TwApp(config.PORT);
    const spy = chai.spy.on(twApp, 'listenLambda');
    twApp.listen();
    spy.should.not.have.been.called();
    twApp.close();
  });
});

import { runned } from './lib/testTaskService';

import TaskModel from '../models/taskModel';
import TwDbConnector from '../twDbConnector';
import twTaskService from '../twTaskService';
import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

import config from './lib/config';
import { testSlowTaskRunnedCounter } from './lib/testSlowTaskService';

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

const twDbConnector = new TwDbConnector(config.MONGO_DB_HOST, config.MONGO_DB_DB);

describe('TwTaskService', function () {
  this.timeout(5000);
  beforeEach((done) => {
    // clear job list
    twTaskService.disable();
    TaskModel.deleteMany({}, (_err) => {
      done();
    });
  });

  before(function (done) {
    twDbConnector.connect();
    done();
  });

  after(function (done) {
    // clear job list
    twTaskService.disable();
    TaskModel.deleteMany({}, () => {
      twDbConnector.disconnect();
      chai.spy.restore();
      done();
    });
  });

  it('it should register a new tasks', async () => {
    const task = new TaskModel({
      name: 'TestTask',
      active: true,
      service_path: './lib/testTaskService'
    });
    await twTaskService.registerTask(task);
    await twTaskService.registerTask(task);
    const tasks = await TaskModel.find();
    tasks.length.should.be.equal(1);
  });

  it('it should enable all tasks', async () => {
    const task = new TaskModel({
      name: 'TestTask',
      active: true,
      service_path: './tests/lib/testTaskService',
      cron_rule: '* * * * *'
    });
    await twTaskService.registerTask(task);
    await twTaskService.enable();

    const tasks = await TaskModel.find();
    tasks.length.should.be.equal(1);

    twTaskService.jobs.length.should.be.equal(1);
    twTaskService.jobs[0].status.should.be.equal('scheduled');
  });

  it('it should disable all tasks', async () => {
    const task = new TaskModel({
      name: 'TestTask',
      active: true,
      service_path: './tests/lib/testTaskService',
      cron_rule: '* * * * *'
    });
    await twTaskService.registerTask(task);
    const task2 = new TaskModel({
      name: 'TestTask2',
      active: true,
      service_path: './tests/lib/testTaskService',
      cron_rule: '2 * * * *'
    });
    await twTaskService.registerTask(task2);
    await twTaskService.enable();
    await twTaskService.disable();
    const tasks = await TaskModel.find();
    tasks.length.should.be.equal(2);
    twTaskService.jobs.length.should.be.equal(0);
  });

  it('it should have called the run function of service', async () => {
    chai.spy.restore();
    const task = new TaskModel({
      name: 'TestTask',
      active: true,
      service_path: './tests/lib/testTaskService',
      cron_rule: '* * * * * *'
    });
    await twTaskService.registerTask(task);
    await twTaskService.enable();
    await timeout(1001);
    runned.should.be.eql(true);
  });

  it('it should not run a task if it is already running', async () => {
    chai.spy.restore();
    const task = new TaskModel({
      name: 'TestSlowTask',
      active: false,
      service_path: './tests/lib/testSlowTaskService',
      cron_rule: '* * * * * *'
    });
    await twTaskService.registerTask(task);
    await twTaskService.enable();
    const slowTask = await twTaskService.getTaskByName(task.name);
    const firstRunPromise = twTaskService.executeTask(slowTask, {});
    await timeout(500);
    await twTaskService.executeTask(slowTask, {});
    await firstRunPromise;
    testSlowTaskRunnedCounter.should.be.eq(1);

    runned.should.be.eql(true);
  });
});

function timeout (ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

import twApp from './lib/app';

import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

describe('Abstract Router with not implemented controller', () => {
  before((done) => {
    twApp.listen();
    twApp.twDbConnector.connect();
    done();
  });
  after((done) => {
    twApp.close().then(() => { done(); });
  });

  describe('/GET', () => {
    it('it should return 501 status', (done) => {
      chai.request(twApp.app)
        .get('/notImplemented')
        .end((_err, res) => {
          res.should.have.status(501);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
  });

  describe('/POST', () => {
    it('it should return 501 status', (done) => {
      chai.request(twApp.app)
        .post('/notImplemented')
        .end((_err, res) => {
          res.should.have.status(501);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
  });

  describe('/PUT', () => {
    it('it should return 501 status', (done) => {
      chai.request(twApp.app)
        .put('/notImplemented/fakeid')
        .end((_err, res) => {
          res.should.have.status(501);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
  });

  describe('/PATCH', () => {
    it('it should return 501 status', (done) => {
      chai.request(twApp.app)
        .patch('/notImplemented/fakeid')
        .end((_err, res) => {
          res.should.have.status(501);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
  });

  describe('/DELETE', () => {
    it('it should return 501 status', (done) => {
      chai.request(twApp.app)
        .delete('/notImplemented/fakeid')
        .end((_err, res) => {
          res.should.have.status(501);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
  });
});

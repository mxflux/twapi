import twApp from './lib/app';
import itemModel from './lib/itemModel';
import DummyFactory from './lib/DummyFactory';

import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

const dummyFactory = new DummyFactory(itemModel);

describe('Abstract Router Update', () => {
  before((done) => {
    twApp.listen();
    twApp.twDbConnector.connect();
    itemModel.deleteMany({}, () => {
      done();
    });
  });
  after((done) => {
    twApp.close().then(() => done());
  });
  beforeEach((done) => {
    dummyFactory.generate(10, done);
  });
  afterEach((done) => {
    itemModel.deleteMany({}, () => {
      done();
    });
  });
  describe('/PUT', () => {
    it('it should not update without id', (done) => {
      const item = { name: dummyFactory.makeString() };
      chai.request(twApp.app)
        .put('/items')
        .send(item)
        .end((_err, res) => {
          res.should.have.status(404);
          done();
        });
    });
    it('it should not update with wrong id', (done) => {
      const item = { name: dummyFactory.makeString() };
      chai.request(twApp.app)
        .put('/items/fakeid')
        .send(item)
        .end((_err, res) => {
          res.should.have.status(404);
          res.get('Content-Length').should.be.equal('0');
          done();
        });
    });
    it('it should not update with missing number', (done) => {
      const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
      const updateItem = { name: dummyFactory.makeString(), number: '' };
      chai.request(twApp.app)
        .put('/items/' + randomItem._id)
        .send(updateItem)
        .end((_err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.number.should.have.property('kind').eql('required');
          done();
        });
    });

    it('it should not update with wrong number type', (done) => {
      const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
      const updateItem = { name: dummyFactory.makeString(), number: 'asdasd' };
      chai.request(twApp.app)
        .put('/items/' + randomItem._id)
        .send(updateItem)
        .end((_err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have.property('errors');
          res.body.errors.number.should.have.property('name').eql('CastError');
          res.body.errors.number.should.have.property('kind').eql('Number');
          done();
        });
    });

    it('it should update the item', (done) => {
      const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
      const updateItem = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber(1, 10000000) };
      chai.request(twApp.app)
        .put('/items/' + randomItem._id)
        .send(updateItem)
        .end((_err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name');
          res.body.should.have.property('number');
          res.body.name.should.be.equal(updateItem.name);
          res.body.number.should.be.equal(updateItem.number);
          done();
        });
    });
  });
});

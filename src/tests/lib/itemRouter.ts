'use strict';

import TwAbstractRouter from "../../twAbstractRouter";
import ItemController from "./itemController";

class ItemRouter extends TwAbstractRouter {
    constructor() {
        super(new ItemController());
    }
}

export default new ItemRouter().getRouter();

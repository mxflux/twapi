"use strict";

import TwAbstractController from "../../twAbstractController";
import itemModel from "./itemModel";

class ItemController extends TwAbstractController{
    constructor(){
        super(itemModel);
    }
}


export default ItemController;



'use strict';

import { Request, Response, NextFunction } from "express";
import itemRouter from "./itemRouter";
import notImplementedRouter from "./notImplementedRouter";
import twRoutingService from "../../twRoutingService";

export default (app: any) => {
    app.use((req: Request, res: Response, next: NextFunction) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
        if (req.method === "OPTIONS") {
            res.header("Access-Control-Allow-Methods", "PUT, POST, DELETE, GET");
            return res.status(200).json({});
        }
        next();
    });
    app.use("/items", itemRouter);
    app.use("/notImplemented", notImplementedRouter);
    twRoutingService.addTaskRoutes(app);
}

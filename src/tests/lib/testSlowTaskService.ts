export default class TestSlowTaskService {
    async run(options: any = null) {
        return new Promise((resolve, reject) => {
            testSlowTaskRunnedCounter++;
            setTimeout(() => {
                resolve( {result: 'task was run'});
            },2000)
        });
    }
}
export let testSlowTaskRunnedCounter = 0;

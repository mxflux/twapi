import config from "./config";
import TwApp from "../../twApp";
import routes from './router';

const twApp = new TwApp(config.PORT);
twApp.connectDB(config.MONGO_DB_HOST, config.MONGO_DB_DB);

// create own router
twApp.addRoutes(routes)
twApp.listen();

export default twApp;
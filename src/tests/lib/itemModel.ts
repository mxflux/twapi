'use strict';

import mongoose, { Schema, Document } from 'mongoose';

interface Item extends Document {
    name: string,
    number: number,
}

const ItemSchema = new Schema({
  name: String,
  number: { type: Number, required: [true, 'Number is required'], unique: true }

});
ItemSchema.add({ subItems: [ItemSchema] });

export default mongoose.model<Item>('Item', ItemSchema);

'use strict';

import TwAbstractRouter from "../../twAbstractRouter";
import NotImplementedController from "./notImplementedController";

class NotImplementedRouter extends TwAbstractRouter {
    constructor() {
        super(new NotImplementedController() as any);
    }
}

export default new NotImplementedRouter().getRouter();

class DummyFactory {

    public dummies: any[];

    private cache: any;

    private dummyValues: any;

    constructor(private model: any) {
        this.dummies = [];
        this.cache = {numbers: [], strings: []};
        this.dummyValues = {};
    }

    generate(count: number, done: Function) {
        this.dummies = [];
        const properties = this.getProperties();
        for (let i = 0; i < count; i++) {
            const dummyData: any = {};
            properties.forEach(property => {
                if (property.path !== '_id' && property.path !== '__v') {
                    dummyData[property.path] = this.getRandom(property.instance);
                }
            });
            const dummy = this.model(dummyData);
            this.dummies.push(dummy);
        }
        // save dummies before done
        this.save(0, done);
    }

    async save(index = 0, done: Function) {
        const dummy = this.dummies[index];
        const result = await dummy.save();
        this.dummies[index] = result;
        if (index < this.dummies.length - 1) {
            await this.save(index + 1, done);
        } else {
            if(done) {
                done();
            }
        }
    }


    getRandom(type: string | null = null, options: any = null): any {
        switch (type) {
            case "Number":
                const fakeNumber = this.makeNumber(1, 10000000);
                if (this.cache.numbers.indexOf(fakeNumber) !== -1) {
                    return this.getRandom(type);
                } else {
                    this.cache.numbers.push(fakeNumber);
                    return fakeNumber;
                }
            case "Date":
                return new Date(this.makeNumber(1980, 2010), this.makeNumber(0, 11), this.makeNumber(1, 28));
            case "Enum":
                return options[this.makeNumber(0, options.length - 1)];
            case "Email":
                return this.makeString()+'@'+this.makeString(5)+'.'+this.makeString(2);
            case "Array":
                return []
            case "ObjectID":
                return null
            case "Boolean":
                return Math.round(Math.random()) ? true : false;
            default:
                const fakeString = this.makeString();
                if (this.cache.strings.indexOf(fakeString) !== -1) {
                    return this.getRandom(type);
                } else {
                    this.cache.strings.push(fakeString);
                    return fakeString;
                }
        }
    }

    getProperties() {
        let properties: any[] = [];
        this.model.schema.eachPath(function (path: any, property: any) {
            properties.push(property);
        });
        return properties;
    }

    makeString(length = 10) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    makeNumber(min = 1, max = 10) {
        max++;
        return (Math.floor(Math.random() * (+max - +min) + +min));
    }
}

export default DummyFactory;
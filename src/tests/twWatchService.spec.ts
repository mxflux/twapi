import TwWatchService from '../twWatchService';

import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);
chai.use(spies);

let twWatchService: TwWatchService = null;

describe('TwWatchService', () => {
  beforeEach(function (done) {
    twWatchService = new TwWatchService({ url: 'dummy', key_id: 'dummy', kkey_secret: 'dummy' });
    done();
  });

  afterEach(function (done) {
    twWatchService.disable();
    done();
  });

  it('should create new instance of twWatchService', function () {
    (typeof twWatchService).should.be.eql('object');
    twWatchService.should.have.property('interval');
  });

  it('should try to send ping to twWatchService server', async function () {
    const spy = chai.spy.on(twWatchService.pingApi, 'post');
    await twWatchService.ping();
    spy.should.have.been.called();
  });
});

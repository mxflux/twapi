import mongoose from 'mongoose';

import twApp from './lib/app';
import TaskModel from '../models/taskModel';
import twTaskService from '../twTaskService';
import { runned } from './lib/testTaskService';
import chai from 'chai';
import chaiHttp = require('chai-http');
import spies = require('chai-spies')
import TaskRun, { TaskRunStatus } from '../models/taskRunModel';

process.env.NODE_ENV = 'test';
chai.use(chaiHttp);
chai.use(spies);

// eslint-disable-next-line no-unused-vars
const should = chai.should();

// add task
const task = new TaskModel({
  name: 'TestTask',
  active: true,
  service_path: './tests/lib/testTaskService',
  cron_rule: '* * * * *',
  maxSkipCount: 2
});

describe('Task Router Test', function () {
  before(async function () {
    // mongoose.models = {};
    (mongoose as any).modelSchemas = {};
    twApp.listen();
    twApp.twDbConnector.connect();
  });
  after(async () => {
    await twApp.close();
  });
  beforeEach(async () => {
    await TaskModel.deleteMany({});
    await TaskRun.deleteMany({});
    await twTaskService.registerTask(task);
    await twTaskService.enable();
  });

  it('it should return one existing job', () => {
    twTaskService.jobs.length.should.be.equal(1);
  });

  it('it should GET all the items', (done) => {
    chai.request(twApp.app)
      .get('/tasks')
      .end((_err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.eql(1);
        done();
      });
  });

  it('it should not add any new tasks', (done) => {
    chai.request(twApp.app)
      .post('/tasks', (task as any))
      .end((_err, res) => {
        res.should.have.status(403);
        res.body.should.be.eql({});
        done();
      });
  });

  it('it should PUT not modify tasks service path and name', async () => {
    const existingTask = await TaskModel.findOne({ name: task.name });
    const res = await chai.request(twApp.app)
      .put('/tasks/' + existingTask._id)
      .send({ name: 'dummyName', service_path: 'wrong_path' });
    res.should.have.status(200);
    res.body.should.be.a('object');
    res.body.name.should.be.eql(task.name);
    const existingTaskAfter = await TaskModel.findOne({ _id: existingTask._id });
    existingTaskAfter.name.should.be.eql(existingTask.name);
    existingTaskAfter.service_path.should.be.eql(existingTask.service_path);
  });

  it('it should PATCH not modify tasks service path and name', (done) => {
    TaskModel.findOne({ name: task.name }).then((existingTask) => {
      chai.request(twApp.app)
        .patch('/tasks/' + existingTask._id)
        .send({ name: 'dummyName', service_path: 'wrong_path' })
        .end((_err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.name.should.be.eql(task.name);
          TaskModel.findOne({ _id: existingTask._id }).then((existingTaskAfter) => {
            existingTaskAfter.name.should.be.eql(existingTask.name);
            existingTaskAfter.service_path.should.be.eql(existingTask.service_path);
            done();
          });
        });
    });
  });

  it('it should execute task', async function () {
    const existingTask = await TaskModel.findOne({ name: task.name });
    const res = await chai.request(twApp.app)
      .post('/tasks/' + existingTask._id + '/run')
      .send();
    res.should.have.status(200);
    runned.should.be.eql(true);
    // spy.should.have.been.called();
  });

  it('it should create task run', async function () {
    const existingTask = await TaskModel.findOne({ name: task.name });
    const res = await chai.request(twApp.app)
      .post('/tasks/' + existingTask._id + '/run')
      .send();
    const taskRuns = await TaskRun.find({ task: existingTask._id });

    res.should.have.status(200);
    res.body.should.be.a('object');
    res.body.should.have.property('task');
    res.body.task.should.be.eq(existingTask._id.toString());

    taskRuns.should.be.a('array');
    taskRuns.length.should.be.eq(1);
    taskRuns[0].should.be.a('object');
    taskRuns[0].should.have.property('status');
    taskRuns[0].status.should.be.a('string');
    taskRuns[0].status.should.be.eq(TaskRunStatus.success);
    taskRuns[0].should.have.property('result');
    taskRuns[0].result.should.be.a('object');
    taskRuns[0].result.should.have.property('result');
    taskRuns[0].result.result.should.be.a('string');
    taskRuns[0].result.result.should.be.eq('task was run');
  });

  it('it should fail to run task', async function () {
    const existingTask = await TaskModel.findOne({ name: task.name });
    const res = await chai.request(twApp.app)
      .post('/tasks/' + existingTask._id + '/run')
      .send({ createError: true });
    res.should.have.status(400);
    const taskRuns = await TaskRun.find({ task: existingTask._id });
    taskRuns.should.be.a('array');
    taskRuns.length.should.be.eq(1);
    taskRuns[0].should.be.a('object');
    taskRuns[0].should.have.property('status');
    taskRuns[0].status.should.be.a('string');
    taskRuns[0].status.should.be.eq(TaskRunStatus.failure);
    taskRuns[0].should.have.property('error');
    taskRuns[0].error.should.be.a('string');
    taskRuns[0].error.should.be.eq('Error: i was thrown');
  });

  it('it should get all task runs', async function () {
    const existingTask = await TaskModel.findOne({ name: task.name });
    await chai.request(twApp.app)
      .post('/tasks/' + existingTask._id + '/run')
      .send();
    await chai.request(twApp.app)
      .post('/tasks/' + existingTask._id + '/run')
      .send({ createError: true });
    const res = await chai.request(twApp.app)
      .get('/tasks/' + existingTask._id + '/run')
      .send();

    res.should.have.status(200);
    res.body.should.be.a('array');
    res.body.length.should.be.eq(2);
    res.body[0].should.be.a('object');
    res.body[0].should.have.property('status');
    res.body[0].status.should.be.a('string');
    res.body[0].status.should.be.eq(TaskRunStatus.success);
    res.body[1].should.be.a('object');
    res.body[1].should.have.property('status');
    res.body[1].status.should.be.a('string');
    res.body[1].status.should.be.eq(TaskRunStatus.failure);
  });

  it('should rerun after 2 retries', async function () {
    const existingTask = await TaskModel.findOne({ name: task.name });
    const taskRun = new TaskRun(
      {
        task: existingTask._id
      }
    );
    await taskRun.save();

    for (let i = 0; i < 3; i++) {
      await chai.request(twApp.app)
        .post('/tasks/' + existingTask._id + '/run')
        .send();
    }

    const taskRuns = await TaskRun.find();
    taskRuns.should.be.a('array');
    taskRuns.length.should.be.eq(2);
    taskRuns[0].should.have.property('skipCount').with.is.a('number');
    taskRuns[0].skipCount.should.be.eq(2);
  });
});

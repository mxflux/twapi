import { Request, Response, Router } from 'express';
import TwAbstractController from './twAbstractController';
import { Document } from 'mongoose';
import TwAbstractRouter from './twAbstractRouter';
import { schedule } from 'node-cron';
import TaskModel, { Task } from './models/taskModel';
import TaskRun, { ITaskRun, TaskRunStatus } from './models/taskRunModel';
import Cloudwatchevents from 'aws-sdk/clients/cloudwatchevents';
import Lambda from 'aws-sdk/clients/lambda';
import nodeCleanup from 'node-cleanup';

export interface ITwTaskService {
    run(options?: any): Promise<void | boolean>
}

async function runTask (task: any, options: any[]): Promise<ITaskRun> {
  let TaskService = await import(task.service_path);
  TaskService = TaskService.default;
  const taskService = new TaskService();

  // check if task is currently running
  const runningTask = await TaskRun.findOne({ task: task._id, status: TaskRunStatus.running });
  if (runningTask) {
    const maxSkipCount = task.maxSkipCount || 5;
    if (runningTask.skipCount >= maxSkipCount) {
      runningTask.status = TaskRunStatus.cancelled;
      await runningTask.save();
    } else {
      runningTask.skipCount++;
      await runningTask.save();
      return runningTask;
    }
  }

  // const taskService = await Promise.resolve().then(() => require(task.service_path));
  const taskRun = new TaskRun();
  taskRun.task = task._id;
  await taskRun.save();
  try {
    if (options) {
      taskRun.result = await taskService.run(options);
    } else {
      taskRun.result = await taskService.run();
    }
    taskRun.status = TaskRunStatus.success;
  } catch (e) {
    taskRun.status = TaskRunStatus.failure;
    taskRun.error = e.toString();
  }
  task.last_run = new Date();
  await task.save();
  await taskRun.save();
  return taskRun;
}

class TwTaskService {
    public jobs: any[] = [];

    async executeTask (task: Task, options: any): Promise<ITaskRun> {
      return await runTask(task, options);
    }

    async getTaskByName (taskName: string): Promise<Task> {
      const task = await TaskModel.findOne({ name: taskName });
      if (!task) {
        throw new Error('Task not found');
      }
      return task;
    }

    async registerTask (task: any): Promise<any> {
      // check if task already exists
      if (!await TaskModel.findOne({ name: task.name })) {
        // add new task if not exists
        const mTask = new TaskModel(task);
        return await mTask.save();
      }
    }

    async enable () {
      const isInLambda = !!process.env.LAMBDA_TASK_ROOT;
      if (isInLambda) {
        return this.enableLambda();
      }
      this.registerCleanup();
      // disable existing jobs first
      await this.disable();
      // get active tasks with existing cron rule
      const tasks = await TaskModel.find({ active: true, cron_rule: { $exists: true } });
      for (let i = 0; i < tasks.length; i++) {
        const task: any = tasks[i];
        const job = schedule(task.cron_rule, async () => {
          await runTask(task, task.options);
        }, {
          scheduled: true
        });
        this.jobs.push(job);
      }
    }

    async registerCleanup () {
      nodeCleanup(function (exitCode, signal) {
        if (signal) {
          TaskRun.updateMany({ status: TaskRunStatus.running }, { status: TaskRunStatus.cancelled });
        }
      });
    }

    async enableLambda () {
      const tasks = await TaskModel.find();
      if (tasks.length) {
        const cloudwatchevents = new Cloudwatchevents();
        const lambda = new Lambda();
        const functionName: string = process.env.AWS_LAMBDA_FUNCTION_NAME;
        const functionResult: any = await lambda.getFunction({ FunctionName: functionName }).promise();

        const functionArn = functionResult.Configuration.FunctionArn;
        // const result: any = (await (cloudwatchevents.listRules({}).promise() as any));
        // const rules: Cloudwatchevents.Rule[] = result.Rules;
        for (const task of tasks) {
          const ruleName = (functionName + '_' + task.name).substring(0, 90);
          // const existingRule = rules.find((r: Cloudwatchevents.Rule) => r.Name === ruleName);
          const cronRule = (task.cron_rule) ? 'cron(' + task.cron_rule + ')' : 'cron(0 1 * * ? *)';
          const ruleParams = {
            Name: ruleName,
            Description: ruleName,
            ScheduleExpression: cronRule,
            State: (task.active) ? 'ENABLED' : 'DISABLED'

          };
          const putResult: any = await cloudwatchevents.putRule(ruleParams).promise();
          if (putResult && putResult.RuleArn) {
            const targetParams = {
              Rule: ruleParams.Name,
              Targets: [
                {
                  Id: ruleParams.Name,
                  Arn: functionArn,
                  Input: '{"service":"' + task.name + '"}'
                }
              ]
            };
            await cloudwatchevents.putTargets(targetParams).promise();

            try {
              await lambda
                .removePermission({
                  FunctionName: functionArn,
                  StatementId: 'allow-rule-invoke-' + ruleName.substring(ruleName.length - 20)
                }).promise();
            } catch (e) {
              console.log('could not remove permission');
            }

            await lambda
              .addPermission({
                Action: 'lambda:invokeFunction',
                FunctionName: functionArn,
                StatementId: 'allow-rule-invoke-' + ruleName.substring(ruleName.length - 20),
                Principal: 'events.amazonaws.com',
                SourceArn: putResult.RuleArn
              })
              .promise();
          } else {
            throw new Error('There was an error creating the Rule');
          }
          console.log('Rule ' + ruleParams.Name + 'successfully created');
        }
      }
    }

    async disable () {
      for (const job of this.jobs) {
        await job.destroy();
      }
      this.jobs = [];
    }

    getTaskRoutes () {
      class TaskController extends TwAbstractController {
        constructor () {
          super(TaskModel);
        }

        async init (req: Request, res: Response) {
          try {
            await instance.enableLambda();
            res.status(200).send();
          } catch (e) {
            res.status(500).send(e);
          }
        }

        async post (req: Request, res: Response) {
          // forbid creation of new tasks thru api
          res.status(403).send();
        }

        async put (req: Request, res: Response) {
          delete req.body.name;
          delete req.body.service_path;
          delete req.body.last_run;
          super.put(req, res);
        }

        async patch (req: Request, res: Response) {
          delete req.body.name;
          delete req.body.service_path;
          delete req.body.last_run;
          super.patch(req, res);
        }

        async saveDocument (document: Document) {
          const task = await super.saveDocument(document);
          await instance.enable();
          return task;
        }

        // deprectated!!!
        async saveModel (model: any) {
          const task = await super.saveDocument(model);
          await instance.enable();
          return task;
        }

        async delete (req: Request, res: Response) {
          super.delete(req, res);
          await instance.enable();
        }

        async runTask (req: Request, res: Response) {
          const id = req.params.id;
          const task = await TaskModel.findOne({ _id: id });
          if (task) {
            const taskRun = await runTask(task, req.body);
            if (taskRun.status === TaskRunStatus.success) {
              res.status(200).json(taskRun);
            } else {
              res.status(400).json(taskRun);
            }
          } else {
            res.status(404).send();
          }
        }

        async listTaskRuns (req: Request, res: Response) {
          const id = req.params.id;
          const taskRuns = await TaskRun.find({ task: id });
          if (taskRuns) {
            res.json(taskRuns);
          } else {
            res.status(404).send();
          }
        }
      }

      class TaskRouter extends TwAbstractRouter {
        constructor () {
          super(new TaskController());
        }

        getRouter () {
          const router = Router();
          router.get('/', (req, res, next) => {
            this.controller.list(req, res, next);
          });

          const isInLambda = !!process.env.LAMBDA_TASK_ROOT;
          if (isInLambda) {
            router.post('/init', (req, res, next) => {
              this.controller.init(req, res, next);
            });
          }

          router.get('/:id', (req, res, next) => {
            this.controller.get(req, res, next);
          });

          router.put('/:id', (req, res, next) => {
            this.controller.put(req, res, next);
          });

          router.patch('/:id', (req, res, next) => {
            this.controller.patch(req, res, next);
          });
          router.post('/', (req, res, next) => {
            this.controller.post(req, res, next);
          });

          router.post('/:id/run', (req, res, next) => {
            this.controller.runTask(req, res, next);
          });

          router.get('/:id/run', (req, res, next) => {
            this.controller.listTaskRuns(req, res, next);
          });

          return router;
        }
      }

      return new TaskRouter().getRouter();
    }
}

const instance = new TwTaskService();

export default instance;

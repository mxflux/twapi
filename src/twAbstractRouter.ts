import { NextFunction, Response, Router } from 'express';
import TwAbstractController from './twAbstractController';

interface Permissions {
    [key: string]: any;
}

class TwAbstractRouter {
    // TO-DO: Find out of what type controller is. (TwAbstract controller supports no next callbacks)
    protected controller: any;

    private permissions: Permissions | null;

    constructor (controller: TwAbstractController | null = null, permissions: Permissions | null = null) {
      this.controller = controller;
      this.permissions = permissions;
    }

    getRequiredRoles (method: string, path: string) {
      if (this.permissions) {
        if (this.permissions[method]) {
          const pathPermission = this.permissions[method].paths.find((rpath: any) => rpath.path === path);
          if (pathPermission) {
            return pathPermission.roles;
          }
          return this.permissions[method].roles;
        }
        return this.permissions.roles;
      }
      return [];
    }

    checkPermissions (roles: any) {
      return (req: any, res: Response, next: NextFunction) => {
        if (roles.length === 0) {
          return next();
        }
        if (req.decoded.roles.some((r: any) => roles.includes(r))) {
          next();
        } else {
          res.status(403).send();
        }
      };
    }

    getRouter () {
      const express = require('express');
      const router = express.Router() as Router;

      router.get('/', this.checkPermissions(this.getRequiredRoles('get', '/')), (req, res, next) => {
        if (this.controller.list) {
          this.controller.list(req, res, next);
        } else {
          res.status(501).send();
        }
      });

      router.get('/:id', this.checkPermissions(this.getRequiredRoles('get', '/:id')), (req, res, next) => {
        if (this.controller.get) {
          this.controller.get(req, res, next);
        } else {
          res.status(501).send();
        }
      });

      router.post('/', this.checkPermissions(this.getRequiredRoles('post', '/')), (req, res, next) => {
        if (this.controller.post) {
          this.controller.post(req, res, next);
        } else {
          res.status(501).send();
        }
      });

      router.put('/:id', this.checkPermissions(this.getRequiredRoles('put', '/:id')), (req, res, next) => {
        if (this.controller.put) {
          this.controller.put(req, res, next);
        } else {
          res.status(501).send();
        }
      });

      router.patch('/:id', this.checkPermissions(this.getRequiredRoles('patch', '/:id')), (req, res, next) => {
        if (this.controller.patch) {
          this.controller.patch(req, res, next);
        } else {
          res.status(501).send();
        }
      });

      router.delete('/:id', this.checkPermissions(this.getRequiredRoles('delete', '/:id')), (req, res, next) => {
        if (this.controller.delete) {
          this.controller.delete(req, res, next);
        } else {
          res.status(501).send();
        }
      });
      return router;
    }
}

export default TwAbstractRouter;

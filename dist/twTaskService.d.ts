import { Task } from './models/taskModel';
import { ITaskRun } from './models/taskRunModel';
export interface ITwTaskService {
    run(options?: any): Promise<void | boolean>;
}
declare class TwTaskService {
    jobs: any[];
    executeTask(task: Task, options: any): Promise<ITaskRun>;
    getTaskByName(taskName: string): Promise<Task>;
    registerTask(task: any): Promise<any>;
    enable(): Promise<void>;
    registerCleanup(): Promise<void>;
    enableLambda(): Promise<void>;
    disable(): Promise<void>;
    getTaskRoutes(): import("express-serve-static-core").Router;
}
declare const instance: TwTaskService;
export default instance;

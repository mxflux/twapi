import { Application, NextFunction, Request, Response } from 'express';
declare class TwRoutingService {
    sendOptions(req: Request, res: Response, next: NextFunction): void;
    addTaskRoutes(app: Application, path?: string): void;
    authorize(req: Request, res: Response, next: NextFunction, jwksUri: string, apiClients?: any[] | null): void;
}
declare const _default: TwRoutingService;
export default _default;

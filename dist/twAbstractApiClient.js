"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const rp = __importStar(require("request-promise"));
class TwAbstractApiClient {
    constructor(uri, keyId = null, keySecret = null) {
        if (new.target === TwAbstractApiClient) {
            throw new TypeError('Cannot construct Abstract instances directly');
        }
        this.uri = uri;
        this.keyId = keyId;
        this.keySecret = keySecret;
    }
    list(query = null, sorting = null, paging = null, projection = null) {
        let queryPath = '';
        if (query || sorting || paging) {
            queryPath += '?';
            const params = [];
            if (Object.keys(query).length) {
                params.push('q=' + encodeURIComponent(JSON.stringify(query)));
            }
            if (sorting) {
                params.push('s=' + encodeURIComponent(JSON.stringify(sorting)));
            }
            if (paging) {
                params.push('p=' + encodeURIComponent(JSON.stringify(paging)));
            }
            if (projection) {
                params.push('o=' + encodeURIComponent(JSON.stringify(projection)));
            }
            queryPath += params.join('&');
        }
        return new Promise((resolve, reject) => {
            const options = this.getOptions();
            options.uri += queryPath;
            rp.get(options).then((res) => {
                resolve(res);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    get(id) {
        return new Promise((resolve, reject) => {
            const options = this.getOptions();
            options.uri += id;
            rp.get(options).then((res) => {
                resolve(res);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    post(data) {
        return new Promise((resolve, reject) => {
            const options = this.getOptions();
            options.method = 'POST';
            options.body = data;
            rp.post(options).then((res) => {
                resolve(res);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    put(id, data) {
        return new Promise((resolve, reject) => {
            const options = this.getOptions();
            options.method = 'PUT';
            options.body = data;
            options.uri += id;
            rp.put(options).then((res) => {
                resolve(res);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    getOptions() {
        const options = {
            json: true,
            uri: this.getUir(),
            headers: {
                'content-type': 'application/json'
            }
        };
        if (this.keyId && this.keySecret) {
            options.headers.authorization = JSON.stringify({
                KEY_ID: this.keyId,
                KEY_SECRET: this.keySecret
            });
        }
        return options;
    }
    getUir() {
        return this.uri.replace(/\/?$/, '/');
    }
}
exports.default = TwAbstractApiClient;
//# sourceMappingURL=twAbstractApiClient.js.map
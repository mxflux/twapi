import mongoose, { Document } from 'mongoose';
export declare enum TaskRunStatus {
    success = "success",
    failure = "failure",
    running = "running",
    cancelled = "cancelled"
}
export interface ITaskRun extends Document {
    task: string;
    status: TaskRunStatus;
    result: any;
    error: any;
    createdAt: Date;
    updatedAt: Date;
    skipCount: number;
}
declare const _default: mongoose.Model<ITaskRun, {}, {}, {}, any>;
export default _default;

import mongoose from 'mongoose';
export interface Task extends mongoose.Document {
    name: string;
    active: boolean;
    service_path: string;
    cron_rule?: string;
    last_run?: Date;
    options?: object;
    maxSkipCount?: number;
}
declare const _default: mongoose.Model<Task, {}, {}, {}, any>;
export default _default;

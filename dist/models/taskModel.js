"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable camelcase */
const mongoose_1 = require("mongoose");
const TaskSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: [true, 'Task Name ist required'],
        unique: true
    },
    active: { type: Boolean, default: false },
    cron_rule: {
        type: String
    },
    service_path: {
        type: String,
        required: [true, 'Path to Service file is required']
    },
    last_run: Date,
    options: Object,
    maxSkipCount: { type: Number, default: 0 }
});
exports.default = mongoose_1.model('Task', TaskSchema);
//# sourceMappingURL=taskModel.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
class TwDbConnector {
    constructor(host, database) {
        this.host = host;
        this.database = database;
        mongoose_1.default.Promise = global.Promise;
    }
    connect() {
        if (!this.connected) {
            this.connected = true;
            mongoose_1.default.connect(this.host, {
                dbName: this.database
            }).then(() => {
                console.log('Connected to MongoDB ' + this.host + '/' + this.database);
            }).catch((err) => {
                console.error(err.toString());
                this.connected = false;
            });
        }
    }
    disconnect() {
        return new Promise((resolve, reject) => {
            this.connected = false;
            mongoose_1.default.disconnect().then(() => {
                console.log(`MongoDB ${this.host} ${this.database} disconnected`);
                resolve();
            }).catch((err) => {
                console.error(err.toString());
                reject(err);
            });
        });
    }
}
exports.default = TwDbConnector;
//# sourceMappingURL=twDbConnector.js.map
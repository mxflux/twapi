"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const twDbConnector_1 = __importDefault(require("./twDbConnector"));
const twWatchService_1 = __importDefault(require("./twWatchService"));
const twTaskService_1 = __importDefault(require("./twTaskService"));
const aws_serverless_express_1 = __importDefault(require("aws-serverless-express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
class TwApp {
    constructor(port = 3000, isInLambda = false) {
        this.port = port;
        this.isInLambda = isInLambda;
        this.app = express_1.default();
        this.app.use(body_parser_1.default.urlencoded({ extended: true, limit: '50mb' }));
        this.app.use(body_parser_1.default.json({ limit: '50mb' }));
        this.app.use(cookie_parser_1.default());
        this.isInLambda = isInLambda;
    }
    connectDB(host, database) {
        this.twDbConnector = new twDbConnector_1.default(host, database);
        this.twDbConnector.connect();
    }
    enableWatchSerive(watchServiceConfig) {
        if (!this.isInLambda) {
            this.twWatchService = new twWatchService_1.default(watchServiceConfig);
        }
    }
    registerTask(task) {
        twTaskService_1.default.registerTask(task);
    }
    enableTasks() {
        if (!this.isInLambda) {
            twTaskService_1.default.enable();
        }
    }
    addRoutes(routes) {
        routes(this.app);
    }
    setPort(port) {
        this.port = port;
    }
    listen() {
        if (this.isInLambda) {
            return this.listenLambda();
        }
        else {
            if (!this.server || !this.server.listening) {
                this.server = this.app.listen(this.port);
                console.log('TwApp: RESTful API server started on: ' + this.port + ' with PID ' + process.pid);
            }
            return this.app;
        }
    }
    listenLambda() {
        const server = aws_serverless_express_1.default.createServer(this.app);
        // eslint-disable-next-line no-return-assign
        return this.app = exports.lambdaHandler = (event, context) => {
            context.callbackWaitsForEmptyEventLoop = false;
            console.log('TwApp: Lambda Server');
            // fix issue with base path url
            if (event.pathParameters && event.pathParameters.proxy) {
                let newPath = '/';
                // add stage if previously included in path
                if (event.path.includes(event.requestContext.stage)) {
                    newPath += event.requestContext.stage + '/';
                }
                newPath += event.pathParameters.proxy;
                event.path = newPath;
            }
            if (event.service) {
                return this.executeTask(event);
            }
            else {
                return aws_serverless_express_1.default.proxy(server, event, context);
            }
        };
    }
    executeTask(event) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('lambda task started');
                const task = yield twTaskService_1.default.getTaskByName(event.service);
                const options = (event.options) ? event.options : task.options;
                yield twTaskService_1.default.executeTask(task, options);
                console.log('lambda task ended');
            }
            catch (e) {
                console.error(e);
                throw e;
            }
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.server) {
                yield this.server.close();
            }
            console.log('closed server');
            if (this.twDbConnector) {
                yield this.twDbConnector.disconnect();
            }
            console.log('closed db');
            if (this.twWatchService) {
                yield this.twWatchService.disable();
            }
            yield twTaskService_1.default.disable();
            console.log('ended');
        });
    }
}
exports.default = TwApp;
//# sourceMappingURL=twApp.js.map
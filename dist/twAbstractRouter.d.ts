import { NextFunction, Response, Router } from 'express';
import TwAbstractController from './twAbstractController';
interface Permissions {
    [key: string]: any;
}
declare class TwAbstractRouter {
    protected controller: any;
    private permissions;
    constructor(controller?: TwAbstractController | null, permissions?: Permissions | null);
    getRequiredRoles(method: string, path: string): any;
    checkPermissions(roles: any): (req: any, res: Response, next: NextFunction) => void;
    getRouter(): Router;
}
export default TwAbstractRouter;

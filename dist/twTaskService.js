"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const twAbstractController_1 = __importDefault(require("./twAbstractController"));
const twAbstractRouter_1 = __importDefault(require("./twAbstractRouter"));
const node_cron_1 = require("node-cron");
const taskModel_1 = __importDefault(require("./models/taskModel"));
const taskRunModel_1 = __importStar(require("./models/taskRunModel"));
const cloudwatchevents_1 = __importDefault(require("aws-sdk/clients/cloudwatchevents"));
const lambda_1 = __importDefault(require("aws-sdk/clients/lambda"));
const node_cleanup_1 = __importDefault(require("node-cleanup"));
function runTask(task, options) {
    return __awaiter(this, void 0, void 0, function* () {
        let TaskService = yield Promise.resolve().then(() => __importStar(require(task.service_path)));
        TaskService = TaskService.default;
        const taskService = new TaskService();
        // check if task is currently running
        const runningTask = yield taskRunModel_1.default.findOne({ task: task._id, status: taskRunModel_1.TaskRunStatus.running });
        if (runningTask) {
            const maxSkipCount = task.maxSkipCount || 5;
            if (runningTask.skipCount >= maxSkipCount) {
                runningTask.status = taskRunModel_1.TaskRunStatus.cancelled;
                yield runningTask.save();
            }
            else {
                runningTask.skipCount++;
                yield runningTask.save();
                return runningTask;
            }
        }
        // const taskService = await Promise.resolve().then(() => require(task.service_path));
        const taskRun = new taskRunModel_1.default();
        taskRun.task = task._id;
        yield taskRun.save();
        try {
            if (options) {
                taskRun.result = yield taskService.run(options);
            }
            else {
                taskRun.result = yield taskService.run();
            }
            taskRun.status = taskRunModel_1.TaskRunStatus.success;
        }
        catch (e) {
            taskRun.status = taskRunModel_1.TaskRunStatus.failure;
            taskRun.error = e.toString();
        }
        task.last_run = new Date();
        yield task.save();
        yield taskRun.save();
        return taskRun;
    });
}
class TwTaskService {
    constructor() {
        this.jobs = [];
    }
    executeTask(task, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield runTask(task, options);
        });
    }
    getTaskByName(taskName) {
        return __awaiter(this, void 0, void 0, function* () {
            const task = yield taskModel_1.default.findOne({ name: taskName });
            if (!task) {
                throw new Error('Task not found');
            }
            return task;
        });
    }
    registerTask(task) {
        return __awaiter(this, void 0, void 0, function* () {
            // check if task already exists
            if (!(yield taskModel_1.default.findOne({ name: task.name }))) {
                // add new task if not exists
                const mTask = new taskModel_1.default(task);
                return yield mTask.save();
            }
        });
    }
    enable() {
        return __awaiter(this, void 0, void 0, function* () {
            const isInLambda = !!process.env.LAMBDA_TASK_ROOT;
            if (isInLambda) {
                return this.enableLambda();
            }
            this.registerCleanup();
            // disable existing jobs first
            yield this.disable();
            // get active tasks with existing cron rule
            const tasks = yield taskModel_1.default.find({ active: true, cron_rule: { $exists: true } });
            for (let i = 0; i < tasks.length; i++) {
                const task = tasks[i];
                const job = node_cron_1.schedule(task.cron_rule, () => __awaiter(this, void 0, void 0, function* () {
                    yield runTask(task, task.options);
                }), {
                    scheduled: true
                });
                this.jobs.push(job);
            }
        });
    }
    registerCleanup() {
        return __awaiter(this, void 0, void 0, function* () {
            node_cleanup_1.default(function (exitCode, signal) {
                if (signal) {
                    taskRunModel_1.default.updateMany({ status: taskRunModel_1.TaskRunStatus.running }, { status: taskRunModel_1.TaskRunStatus.cancelled });
                }
            });
        });
    }
    enableLambda() {
        return __awaiter(this, void 0, void 0, function* () {
            const tasks = yield taskModel_1.default.find();
            if (tasks.length) {
                const cloudwatchevents = new cloudwatchevents_1.default();
                const lambda = new lambda_1.default();
                const functionName = process.env.AWS_LAMBDA_FUNCTION_NAME;
                const functionResult = yield lambda.getFunction({ FunctionName: functionName }).promise();
                const functionArn = functionResult.Configuration.FunctionArn;
                // const result: any = (await (cloudwatchevents.listRules({}).promise() as any));
                // const rules: Cloudwatchevents.Rule[] = result.Rules;
                for (const task of tasks) {
                    const ruleName = (functionName + '_' + task.name).substring(0, 90);
                    // const existingRule = rules.find((r: Cloudwatchevents.Rule) => r.Name === ruleName);
                    const cronRule = (task.cron_rule) ? 'cron(' + task.cron_rule + ')' : 'cron(0 1 * * ? *)';
                    const ruleParams = {
                        Name: ruleName,
                        Description: ruleName,
                        ScheduleExpression: cronRule,
                        State: (task.active) ? 'ENABLED' : 'DISABLED'
                    };
                    const putResult = yield cloudwatchevents.putRule(ruleParams).promise();
                    if (putResult && putResult.RuleArn) {
                        const targetParams = {
                            Rule: ruleParams.Name,
                            Targets: [
                                {
                                    Id: ruleParams.Name,
                                    Arn: functionArn,
                                    Input: '{"service":"' + task.name + '"}'
                                }
                            ]
                        };
                        yield cloudwatchevents.putTargets(targetParams).promise();
                        try {
                            yield lambda
                                .removePermission({
                                FunctionName: functionArn,
                                StatementId: 'allow-rule-invoke-' + ruleName.substring(ruleName.length - 20)
                            }).promise();
                        }
                        catch (e) {
                            console.log('could not remove permission');
                        }
                        yield lambda
                            .addPermission({
                            Action: 'lambda:invokeFunction',
                            FunctionName: functionArn,
                            StatementId: 'allow-rule-invoke-' + ruleName.substring(ruleName.length - 20),
                            Principal: 'events.amazonaws.com',
                            SourceArn: putResult.RuleArn
                        })
                            .promise();
                    }
                    else {
                        throw new Error('There was an error creating the Rule');
                    }
                    console.log('Rule ' + ruleParams.Name + 'successfully created');
                }
            }
        });
    }
    disable() {
        return __awaiter(this, void 0, void 0, function* () {
            for (const job of this.jobs) {
                yield job.destroy();
            }
            this.jobs = [];
        });
    }
    getTaskRoutes() {
        class TaskController extends twAbstractController_1.default {
            constructor() {
                super(taskModel_1.default);
            }
            init(req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        yield instance.enableLambda();
                        res.status(200).send();
                    }
                    catch (e) {
                        res.status(500).send(e);
                    }
                });
            }
            post(req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    // forbid creation of new tasks thru api
                    res.status(403).send();
                });
            }
            put(req, res) {
                const _super = Object.create(null, {
                    put: { get: () => super.put }
                });
                return __awaiter(this, void 0, void 0, function* () {
                    delete req.body.name;
                    delete req.body.service_path;
                    delete req.body.last_run;
                    _super.put.call(this, req, res);
                });
            }
            patch(req, res) {
                const _super = Object.create(null, {
                    patch: { get: () => super.patch }
                });
                return __awaiter(this, void 0, void 0, function* () {
                    delete req.body.name;
                    delete req.body.service_path;
                    delete req.body.last_run;
                    _super.patch.call(this, req, res);
                });
            }
            saveDocument(document) {
                const _super = Object.create(null, {
                    saveDocument: { get: () => super.saveDocument }
                });
                return __awaiter(this, void 0, void 0, function* () {
                    const task = yield _super.saveDocument.call(this, document);
                    yield instance.enable();
                    return task;
                });
            }
            // deprectated!!!
            saveModel(model) {
                const _super = Object.create(null, {
                    saveDocument: { get: () => super.saveDocument }
                });
                return __awaiter(this, void 0, void 0, function* () {
                    const task = yield _super.saveDocument.call(this, model);
                    yield instance.enable();
                    return task;
                });
            }
            delete(req, res) {
                const _super = Object.create(null, {
                    delete: { get: () => super.delete }
                });
                return __awaiter(this, void 0, void 0, function* () {
                    _super.delete.call(this, req, res);
                    yield instance.enable();
                });
            }
            runTask(req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    const id = req.params.id;
                    const task = yield taskModel_1.default.findOne({ _id: id });
                    if (task) {
                        const taskRun = yield runTask(task, req.body);
                        if (taskRun.status === taskRunModel_1.TaskRunStatus.success) {
                            res.status(200).json(taskRun);
                        }
                        else {
                            res.status(400).json(taskRun);
                        }
                    }
                    else {
                        res.status(404).send();
                    }
                });
            }
            listTaskRuns(req, res) {
                return __awaiter(this, void 0, void 0, function* () {
                    const id = req.params.id;
                    const taskRuns = yield taskRunModel_1.default.find({ task: id });
                    if (taskRuns) {
                        res.json(taskRuns);
                    }
                    else {
                        res.status(404).send();
                    }
                });
            }
        }
        class TaskRouter extends twAbstractRouter_1.default {
            constructor() {
                super(new TaskController());
            }
            getRouter() {
                const router = express_1.Router();
                router.get('/', (req, res, next) => {
                    this.controller.list(req, res, next);
                });
                const isInLambda = !!process.env.LAMBDA_TASK_ROOT;
                if (isInLambda) {
                    router.post('/init', (req, res, next) => {
                        this.controller.init(req, res, next);
                    });
                }
                router.get('/:id', (req, res, next) => {
                    this.controller.get(req, res, next);
                });
                router.put('/:id', (req, res, next) => {
                    this.controller.put(req, res, next);
                });
                router.patch('/:id', (req, res, next) => {
                    this.controller.patch(req, res, next);
                });
                router.post('/', (req, res, next) => {
                    this.controller.post(req, res, next);
                });
                router.post('/:id/run', (req, res, next) => {
                    this.controller.runTask(req, res, next);
                });
                router.get('/:id/run', (req, res, next) => {
                    this.controller.listTaskRuns(req, res, next);
                });
                return router;
            }
        }
        return new TaskRouter().getRouter();
    }
}
const instance = new TwTaskService();
exports.default = instance;
//# sourceMappingURL=twTaskService.js.map
import TwAbstractApiClient from './twAbstractApiClient';
declare class TwWatchPingApi extends TwAbstractApiClient {
    constructor(uri: string, keyId: string, keySecret: string);
}
declare class TwWatchService {
    private config;
    private status;
    pingApi: TwWatchPingApi;
    private interval;
    constructor(config: any);
    ping(): Promise<void>;
    disable(): void;
}
export default TwWatchService;

interface Header {
    'content-type': string;
    authorization?: string;
}
interface Option {
    json: string;
    headers: Header;
    method?: string;
    uri: string;
    body?: any;
}
declare class TwAbstractApiClient {
    private uri;
    private keyId;
    private keySecret;
    constructor(uri: string, keyId?: string | null, keySecret?: string | null);
    list(query?: any | null, sorting?: string | null, paging?: string | null, projection?: string | null): Promise<any[]>;
    get(id: string): Promise<any>;
    post(data: any): Promise<any>;
    put(id: string, data: any): Promise<any>;
    getOptions(): Option;
    getUir(): string;
}
export default TwAbstractApiClient;

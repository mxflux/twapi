"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = __importStar(require("jsonwebtoken"));
const twTaskService_1 = __importDefault(require("./twTaskService"));
const jwksClient = require('jwks-rsa');
/**
 * check if authorization params are key and secret json string
 * if that is the case check if key and secret are known
 * @param req
 * @param res
 * @param next
 * @param apiClients
 * @returns {boolean}
 */
function authorizeApiToken(req, res, next, apiClients) {
    try {
        const authCredentials = JSON.parse(req.headers.authorization);
        if ((authCredentials.KEY_ID && authCredentials.KEY_SECRET) || (authCredentials.key_id && authCredentials.key_secret)) {
            const keyId = (authCredentials.KEY_ID) ? authCredentials.KEY_ID : authCredentials.key_id;
            const keySecret = (authCredentials.KEY_SECRET) ? authCredentials.KEY_SECRET : authCredentials.key_secret;
            if (apiClients.filter(apiClient => apiClient.key_id === keyId && apiClient.key_secret === keySecret)
                .length === 1) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    catch (e) {
        // Not an api request
        return false;
    }
}
/**
 * check if authorization can be done via jwt
 * @param req
 * @param res
 * @param next
 * @param jwksUri
 */
function authorizeJwtToken(req, res, next, jwksUri) {
    const client = jwksClient({
        jwksUri: jwksUri,
        cache: true
    });
    function getKey(header, callback) {
        try {
            client.getSigningKey(header.kid, function (err, key) {
                if (!key) {
                    console.error('JWK Key not found', err);
                    return callback(new Error('key not found'));
                }
                const signingKey = key.publicKey || key.rsaPublicKey;
                callback(null, signingKey);
            });
        }
        catch (e) {
            callback(e);
        }
    }
    let jwtToken = '';
    if (req.headers.authorization) {
        req.headers.authorization = req.headers.authorization.replace('Bearer ', '');
        jwtToken = req.headers.authorization;
    }
    else if (req.cookies.mxcon_accesstoken) {
        jwtToken = req.cookies.mxcon_accesstoken;
    }
    jwt.verify(jwtToken, getKey, {}, function (_err, decoded) {
        if (!decoded) {
            return res.status(401).send();
        }
        else {
            req.decoded = decoded;
            next();
        }
    });
}
class TwRoutingService {
    sendOptions(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        if (req.method === 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET, PATCH');
            res.status(200).send();
            return;
        }
        next();
    }
    addTaskRoutes(app, path = '/tasks') {
        app.use(path, twTaskService_1.default.getTaskRoutes());
    }
    authorize(req, res, next, jwksUri, apiClients = null) {
        if (!req.headers.authorization && !req.cookies.mxcon_accesstoken) {
            res.status(401).send();
            return;
        }
        // 1. try to use api key and secret to authenticate
        if (apiClients) {
            if (authorizeApiToken(req, res, next, apiClients)) {
                next();
                return;
            }
        }
        // 2. try to use jwt token to authenticate
        authorizeJwtToken(req, res, next, jwksUri);
    }
}
exports.default = new TwRoutingService();
//# sourceMappingURL=twRoutingService.js.map
export interface Indexable {
    [key: string]: string | number;
}

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
const si = require('systeminformation');
class TwWatchPingApi extends twAbstractApiClient_1.default {
    constructor(uri, keyId, keySecret) {
        super(uri + 'pings/', keyId, keySecret);
    }
}
class TwPing {
    constructor(input) {
        this.app = input.app;
        this.status = input.status;
        this.system_info = input.system_info;
    }
}
class TwWatchService {
    constructor(config) {
        this.config = config;
        this.status = 'normal';
        this.pingApi = new TwWatchPingApi(this.config.url, this.config.key_id, this.config.key_secret);
        const interval = (this.config.interval) ? this.config.interval : 60;
        this.interval = setInterval(this.ping.bind(this), (interval * 1000));
        console.log('TwWatchService: Started');
    }
    ping() {
        return __awaiter(this, void 0, void 0, function* () {
            let pjson = 'unknown';
            try {
                pjson = require.main.require('../package.json');
            }
            catch (e) {
                console.error('TwWatchService: Could not find package.json');
            }
            const system_info = {};
            system_info.cpu = yield si.currentLoad();
            system_info.memory = yield si.mem();
            const ping = new TwPing({
                app: pjson.name,
                status: this.status,
                system_info: system_info
            });
            yield this.pingApi.post(ping).catch(() => {
                console.log('TwWatchService: Error while sending ping to tw watch server.');
            });
        });
    }
    disable() {
        console.log('TwWatchService: Stopped');
        clearInterval(this.interval);
    }
}
exports.default = TwWatchService;
//# sourceMappingURL=twWatchService.js.map
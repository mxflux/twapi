"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TwAbstractRouter {
    constructor(controller = null, permissions = null) {
        this.controller = controller;
        this.permissions = permissions;
    }
    getRequiredRoles(method, path) {
        if (this.permissions) {
            if (this.permissions[method]) {
                const pathPermission = this.permissions[method].paths.find((rpath) => rpath.path === path);
                if (pathPermission) {
                    return pathPermission.roles;
                }
                return this.permissions[method].roles;
            }
            return this.permissions.roles;
        }
        return [];
    }
    checkPermissions(roles) {
        return (req, res, next) => {
            if (roles.length === 0) {
                return next();
            }
            if (req.decoded.roles.some((r) => roles.includes(r))) {
                next();
            }
            else {
                res.status(403).send();
            }
        };
    }
    getRouter() {
        const express = require('express');
        const router = express.Router();
        router.get('/', this.checkPermissions(this.getRequiredRoles('get', '/')), (req, res, next) => {
            if (this.controller.list) {
                this.controller.list(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        router.get('/:id', this.checkPermissions(this.getRequiredRoles('get', '/:id')), (req, res, next) => {
            if (this.controller.get) {
                this.controller.get(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        router.post('/', this.checkPermissions(this.getRequiredRoles('post', '/')), (req, res, next) => {
            if (this.controller.post) {
                this.controller.post(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        router.put('/:id', this.checkPermissions(this.getRequiredRoles('put', '/:id')), (req, res, next) => {
            if (this.controller.put) {
                this.controller.put(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        router.patch('/:id', this.checkPermissions(this.getRequiredRoles('patch', '/:id')), (req, res, next) => {
            if (this.controller.patch) {
                this.controller.patch(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        router.delete('/:id', this.checkPermissions(this.getRequiredRoles('delete', '/:id')), (req, res, next) => {
            if (this.controller.delete) {
                this.controller.delete(req, res, next);
            }
            else {
                res.status(501).send();
            }
        });
        return router;
    }
}
exports.default = TwAbstractRouter;
//# sourceMappingURL=twAbstractRouter.js.map
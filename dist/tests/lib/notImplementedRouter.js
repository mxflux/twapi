'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const twAbstractRouter_1 = __importDefault(require("../../twAbstractRouter"));
const notImplementedController_1 = __importDefault(require("./notImplementedController"));
class NotImplementedRouter extends twAbstractRouter_1.default {
    constructor() {
        super(new notImplementedController_1.default());
    }
}
exports.default = new NotImplementedRouter().getRouter();
//# sourceMappingURL=notImplementedRouter.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.testSlowTaskRunnedCounter = void 0;
class TestSlowTaskService {
    run(options = null) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                exports.testSlowTaskRunnedCounter++;
                setTimeout(() => {
                    resolve({ result: 'task was run' });
                }, 2000);
            });
        });
    }
}
exports.default = TestSlowTaskService;
exports.testSlowTaskRunnedCounter = 0;
//# sourceMappingURL=testSlowTaskService.js.map
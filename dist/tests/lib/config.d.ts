declare const _default: {
    MONGO_DB_HOST: string;
    MONGO_DB_DB: string;
    PORT: number;
};
export default _default;

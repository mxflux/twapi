'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const itemRouter_1 = __importDefault(require("./itemRouter"));
const notImplementedRouter_1 = __importDefault(require("./notImplementedRouter"));
const twRoutingService_1 = __importDefault(require("../../twRoutingService"));
exports.default = (app) => {
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        if (req.method === "OPTIONS") {
            res.header("Access-Control-Allow-Methods", "PUT, POST, DELETE, GET");
            return res.status(200).json({});
        }
        next();
    });
    app.use("/items", itemRouter_1.default);
    app.use("/notImplemented", notImplementedRouter_1.default);
    twRoutingService_1.default.addTaskRoutes(app);
};
//# sourceMappingURL=router.js.map
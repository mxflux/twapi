declare class DummyFactory {
    private model;
    dummies: any[];
    private cache;
    private dummyValues;
    constructor(model: any);
    generate(count: number, done: Function): void;
    save(index: number, done: Function): Promise<void>;
    getRandom(type?: string | null, options?: any): any;
    getProperties(): any[];
    makeString(length?: number): string;
    makeNumber(min?: number, max?: number): number;
}
export default DummyFactory;

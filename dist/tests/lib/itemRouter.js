'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const twAbstractRouter_1 = __importDefault(require("../../twAbstractRouter"));
const itemController_1 = __importDefault(require("./itemController"));
class ItemRouter extends twAbstractRouter_1.default {
    constructor() {
        super(new itemController_1.default());
    }
}
exports.default = new ItemRouter().getRouter();
//# sourceMappingURL=itemRouter.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class DummyFactory {
    constructor(model) {
        this.model = model;
        this.dummies = [];
        this.cache = { numbers: [], strings: [] };
        this.dummyValues = {};
    }
    generate(count, done) {
        this.dummies = [];
        const properties = this.getProperties();
        for (let i = 0; i < count; i++) {
            const dummyData = {};
            properties.forEach(property => {
                if (property.path !== '_id' && property.path !== '__v') {
                    dummyData[property.path] = this.getRandom(property.instance);
                }
            });
            const dummy = this.model(dummyData);
            this.dummies.push(dummy);
        }
        // save dummies before done
        this.save(0, done);
    }
    save(index = 0, done) {
        return __awaiter(this, void 0, void 0, function* () {
            const dummy = this.dummies[index];
            const result = yield dummy.save();
            this.dummies[index] = result;
            if (index < this.dummies.length - 1) {
                yield this.save(index + 1, done);
            }
            else {
                if (done) {
                    done();
                }
            }
        });
    }
    getRandom(type = null, options = null) {
        switch (type) {
            case "Number":
                const fakeNumber = this.makeNumber(1, 10000000);
                if (this.cache.numbers.indexOf(fakeNumber) !== -1) {
                    return this.getRandom(type);
                }
                else {
                    this.cache.numbers.push(fakeNumber);
                    return fakeNumber;
                }
            case "Date":
                return new Date(this.makeNumber(1980, 2010), this.makeNumber(0, 11), this.makeNumber(1, 28));
            case "Enum":
                return options[this.makeNumber(0, options.length - 1)];
            case "Email":
                return this.makeString() + '@' + this.makeString(5) + '.' + this.makeString(2);
            case "Array":
                return [];
            case "ObjectID":
                return null;
            case "Boolean":
                return Math.round(Math.random()) ? true : false;
            default:
                const fakeString = this.makeString();
                if (this.cache.strings.indexOf(fakeString) !== -1) {
                    return this.getRandom(type);
                }
                else {
                    this.cache.strings.push(fakeString);
                    return fakeString;
                }
        }
    }
    getProperties() {
        let properties = [];
        this.model.schema.eachPath(function (path, property) {
            properties.push(property);
        });
        return properties;
    }
    makeString(length = 10) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    makeNumber(min = 1, max = 10) {
        max++;
        return (Math.floor(Math.random() * (+max - +min) + +min));
    }
}
exports.default = DummyFactory;
//# sourceMappingURL=DummyFactory.js.map
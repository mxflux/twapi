import mongoose, { Document } from 'mongoose';
interface Item extends Document {
    name: string;
    number: number;
}
declare const _default: mongoose.Model<Item, {}, {}, {}, any>;
export default _default;

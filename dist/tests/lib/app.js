"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("./config"));
const twApp_1 = __importDefault(require("../../twApp"));
const router_1 = __importDefault(require("./router"));
const twApp = new twApp_1.default(config_1.default.PORT);
twApp.connectDB(config_1.default.MONGO_DB_HOST, config_1.default.MONGO_DB_DB);
// create own router
twApp.addRoutes(router_1.default);
twApp.listen();
exports.default = twApp;
//# sourceMappingURL=app.js.map
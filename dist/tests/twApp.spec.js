"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const twApp_1 = __importDefault(require("../twApp"));
const config_1 = __importDefault(require("./lib/config"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
describe('TwApp', () => {
    it('it should create new TwApp with default port', () => {
        const twApp = new twApp_1.default();
        twApp.port.should.be.equal(3000);
    });
    it('it should create new TwApp with custom port', () => {
        const twApp = new twApp_1.default(config_1.default.PORT + 1);
        twApp.port.should.be.equal(config_1.default.PORT + 1);
    });
    it('it should set a new port', () => {
        const twApp = new twApp_1.default();
        twApp.setPort(2000);
        twApp.port.should.be.equal(2000);
    });
    it('it should create new TwApp with lambda environment', () => {
        const twApp = new twApp_1.default(config_1.default.PORT, true);
        twApp.port.should.be.equal(config_1.default.PORT);
        twApp.isInLambda.should.be.equal(true);
    });
    it('it should listen as lamba server', () => {
        const twApp = new twApp_1.default(config_1.default.PORT, true);
        const spy = chai_1.default.spy.on(twApp, 'listenLambda');
        twApp.listen();
        spy.should.have.been.called();
    });
    it('it should listen as express server', () => {
        const twApp = new twApp_1.default(config_1.default.PORT);
        const spy = chai_1.default.spy.on(twApp, 'listenLambda');
        twApp.listen();
        spy.should.not.have.been.called();
        twApp.close();
    });
});
//# sourceMappingURL=twApp.spec.js.map
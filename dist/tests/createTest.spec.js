"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const DummyFactory_1 = __importDefault(require("./lib/DummyFactory"));
const app_1 = __importDefault(require("./lib/app"));
const itemModel_1 = __importDefault(require("./lib/itemModel"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
process.env.NODE_ENV = 'test';
// mongoose.models = {};
mongoose_1.default.modelSchemas = {};
const app = app_1.default.app;
chai_1.default.use(require('chai-http'));
const dummyFactory = new DummyFactory_1.default(itemModel_1.default);
describe('Abstract Router Create', () => {
    before((done) => {
        app_1.default.listen();
        app_1.default.twDbConnector.connect();
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    after((done) => {
        app_1.default.close().then(() => done());
    });
    afterEach((done) => {
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    describe('/POST', () => {
        it('it should not create item without number', (done) => {
            const item = { name: dummyFactory.makeString() };
            chai_1.default.request(app)
                .post('/items')
                .send(item)
                .end((_err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.number.should.have.property('kind').eql('required');
                done();
            });
        });
        it('it should not create item with same number', (done) => {
            const item = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber() };
            const mItem = new itemModel_1.default(item);
            mItem.save().then((mItem) => {
                chai_1.default.request(app)
                    .post('/items')
                    .send(item)
                    .end((_err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.have.property('message');
                    done();
                });
            });
        });
        it('it should create item', (done) => {
            const item = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber() };
            chai_1.default.request(app)
                .post('/items')
                .send(item)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('number');
                res.body.should.have.property('_id');
                res.body.name.should.be.equal(item.name);
                res.body.number.should.be.equal(item.number);
                done();
            });
        });
    });
});
//# sourceMappingURL=createTest.spec.js.map
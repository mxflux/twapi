"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const twWatchService_1 = __importDefault(require("../twWatchService"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
let twWatchService = null;
describe('TwWatchService', () => {
    beforeEach(function (done) {
        twWatchService = new twWatchService_1.default({ url: 'dummy', key_id: 'dummy', kkey_secret: 'dummy' });
        done();
    });
    afterEach(function (done) {
        twWatchService.disable();
        done();
    });
    it('should create new instance of twWatchService', function () {
        (typeof twWatchService).should.be.eql('object');
        twWatchService.should.have.property('interval');
    });
    it('should try to send ping to twWatchService server', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const spy = chai_1.default.spy.on(twWatchService.pingApi, 'post');
            yield twWatchService.ping();
            spy.should.have.been.called();
        });
    });
});
//# sourceMappingURL=twWatchService.spec.js.map
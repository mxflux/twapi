"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./lib/app"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
describe('Abstract Router with not implemented controller', () => {
    before((done) => {
        app_1.default.listen();
        app_1.default.twDbConnector.connect();
        done();
    });
    after((done) => {
        app_1.default.close().then(() => { done(); });
    });
    describe('/GET', () => {
        it('it should return 501 status', (done) => {
            chai_1.default.request(app_1.default.app)
                .get('/notImplemented')
                .end((_err, res) => {
                res.should.have.status(501);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
    });
    describe('/POST', () => {
        it('it should return 501 status', (done) => {
            chai_1.default.request(app_1.default.app)
                .post('/notImplemented')
                .end((_err, res) => {
                res.should.have.status(501);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
    });
    describe('/PUT', () => {
        it('it should return 501 status', (done) => {
            chai_1.default.request(app_1.default.app)
                .put('/notImplemented/fakeid')
                .end((_err, res) => {
                res.should.have.status(501);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
    });
    describe('/PATCH', () => {
        it('it should return 501 status', (done) => {
            chai_1.default.request(app_1.default.app)
                .patch('/notImplemented/fakeid')
                .end((_err, res) => {
                res.should.have.status(501);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
    });
    describe('/DELETE', () => {
        it('it should return 501 status', (done) => {
            chai_1.default.request(app_1.default.app)
                .delete('/notImplemented/fakeid')
                .end((_err, res) => {
                res.should.have.status(501);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
    });
});
//# sourceMappingURL=notImplemented.spec.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./lib/app"));
const itemModel_1 = __importDefault(require("./lib/itemModel"));
const DummyFactory_1 = __importDefault(require("./lib/DummyFactory"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
const dummyFactory = new DummyFactory_1.default(itemModel_1.default);
describe('Abstract Router Update', () => {
    before((done) => {
        app_1.default.listen();
        app_1.default.twDbConnector.connect();
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    after((done) => {
        app_1.default.close().then(() => done());
    });
    beforeEach((done) => {
        dummyFactory.generate(10, done);
    });
    afterEach((done) => {
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    describe('/PUT', () => {
        it('it should not update without id', (done) => {
            const item = { name: dummyFactory.makeString() };
            chai_1.default.request(app_1.default.app)
                .put('/items')
                .send(item)
                .end((_err, res) => {
                res.should.have.status(404);
                done();
            });
        });
        it('it should not update with wrong id', (done) => {
            const item = { name: dummyFactory.makeString() };
            chai_1.default.request(app_1.default.app)
                .put('/items/fakeid')
                .send(item)
                .end((_err, res) => {
                res.should.have.status(404);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
        it('it should not update with missing number', (done) => {
            const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
            const updateItem = { name: dummyFactory.makeString(), number: '' };
            chai_1.default.request(app_1.default.app)
                .put('/items/' + randomItem._id)
                .send(updateItem)
                .end((_err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.errors.number.should.have.property('kind').eql('required');
                done();
            });
        });
        it('it should not update with wrong number type', (done) => {
            const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
            const updateItem = { name: dummyFactory.makeString(), number: 'asdasd' };
            chai_1.default.request(app_1.default.app)
                .put('/items/' + randomItem._id)
                .send(updateItem)
                .end((_err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('errors');
                res.body.should.have.property('errors');
                res.body.errors.number.should.have.property('name').eql('CastError');
                res.body.errors.number.should.have.property('kind').eql('Number');
                done();
            });
        });
        it('it should update the item', (done) => {
            const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
            const updateItem = { name: dummyFactory.makeString(), number: dummyFactory.makeNumber(1, 10000000) };
            chai_1.default.request(app_1.default.app)
                .put('/items/' + randomItem._id)
                .send(updateItem)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('number');
                res.body.name.should.be.equal(updateItem.name);
                res.body.number.should.be.equal(updateItem.number);
                done();
            });
        });
    });
});
//# sourceMappingURL=updateTest.spec.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const DummyFactory_1 = __importDefault(require("./lib/DummyFactory"));
const app_1 = __importDefault(require("./lib/app"));
const itemModel_1 = __importDefault(require("./lib/itemModel"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
const dummyFactory = new DummyFactory_1.default(itemModel_1.default);
describe('Abstract Router Get', () => {
    before((done) => {
        app_1.default.listen();
        app_1.default.twDbConnector.connect();
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    after((done) => {
        app_1.default.close().then(() => done());
    });
    beforeEach((done) => {
        dummyFactory.generate(10, done);
    });
    afterEach((done) => {
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    describe('/GET', () => {
        it('it should GET no items', done => {
            chai_1.default.request(app_1.default.app)
                .get('/items/fakeid')
                .end((_err, res) => {
                res.should.have.status(404);
                res.get('Content-Length').should.be.equal('0');
                done();
            });
        });
        it('it should return item', (done) => {
            const randomItem = dummyFactory.dummies[dummyFactory.makeNumber(0, dummyFactory.dummies.length - 1)];
            chai_1.default.request(app_1.default.app)
                .get('/items/' + randomItem._id)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('number');
                res.body.name.should.be.equal(randomItem.name);
                res.body.number.should.be.equal(randomItem.number);
                done();
            });
        });
    });
});
//# sourceMappingURL=getTest.spec.js.map
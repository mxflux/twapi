"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const app_1 = __importDefault(require("./lib/app"));
const taskModel_1 = __importDefault(require("../models/taskModel"));
const twTaskService_1 = __importDefault(require("../twTaskService"));
const testTaskService_1 = require("./lib/testTaskService");
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
const taskRunModel_1 = __importStar(require("../models/taskRunModel"));
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
// eslint-disable-next-line no-unused-vars
const should = chai_1.default.should();
// add task
const task = new taskModel_1.default({
    name: 'TestTask',
    active: true,
    service_path: './tests/lib/testTaskService',
    cron_rule: '* * * * *',
    maxSkipCount: 2
});
describe('Task Router Test', function () {
    before(function () {
        return __awaiter(this, void 0, void 0, function* () {
            // mongoose.models = {};
            mongoose_1.default.modelSchemas = {};
            app_1.default.listen();
            app_1.default.twDbConnector.connect();
        });
    });
    after(() => __awaiter(this, void 0, void 0, function* () {
        yield app_1.default.close();
    }));
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        yield taskModel_1.default.deleteMany({});
        yield taskRunModel_1.default.deleteMany({});
        yield twTaskService_1.default.registerTask(task);
        yield twTaskService_1.default.enable();
    }));
    it('it should return one existing job', () => {
        twTaskService_1.default.jobs.length.should.be.equal(1);
    });
    it('it should GET all the items', (done) => {
        chai_1.default.request(app_1.default.app)
            .get('/tasks')
            .end((_err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eql(1);
            done();
        });
    });
    it('it should not add any new tasks', (done) => {
        chai_1.default.request(app_1.default.app)
            .post('/tasks', task)
            .end((_err, res) => {
            res.should.have.status(403);
            res.body.should.be.eql({});
            done();
        });
    });
    it('it should PUT not modify tasks service path and name', () => __awaiter(this, void 0, void 0, function* () {
        const existingTask = yield taskModel_1.default.findOne({ name: task.name });
        const res = yield chai_1.default.request(app_1.default.app)
            .put('/tasks/' + existingTask._id)
            .send({ name: 'dummyName', service_path: 'wrong_path' });
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.name.should.be.eql(task.name);
        const existingTaskAfter = yield taskModel_1.default.findOne({ _id: existingTask._id });
        existingTaskAfter.name.should.be.eql(existingTask.name);
        existingTaskAfter.service_path.should.be.eql(existingTask.service_path);
    }));
    it('it should PATCH not modify tasks service path and name', (done) => {
        taskModel_1.default.findOne({ name: task.name }).then((existingTask) => {
            chai_1.default.request(app_1.default.app)
                .patch('/tasks/' + existingTask._id)
                .send({ name: 'dummyName', service_path: 'wrong_path' })
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.name.should.be.eql(task.name);
                taskModel_1.default.findOne({ _id: existingTask._id }).then((existingTaskAfter) => {
                    existingTaskAfter.name.should.be.eql(existingTask.name);
                    existingTaskAfter.service_path.should.be.eql(existingTask.service_path);
                    done();
                });
            });
        });
    });
    it('it should execute task', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const existingTask = yield taskModel_1.default.findOne({ name: task.name });
            const res = yield chai_1.default.request(app_1.default.app)
                .post('/tasks/' + existingTask._id + '/run')
                .send();
            res.should.have.status(200);
            testTaskService_1.runned.should.be.eql(true);
            // spy.should.have.been.called();
        });
    });
    it('it should create task run', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const existingTask = yield taskModel_1.default.findOne({ name: task.name });
            const res = yield chai_1.default.request(app_1.default.app)
                .post('/tasks/' + existingTask._id + '/run')
                .send();
            const taskRuns = yield taskRunModel_1.default.find({ task: existingTask._id });
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('task');
            res.body.task.should.be.eq(existingTask._id.toString());
            taskRuns.should.be.a('array');
            taskRuns.length.should.be.eq(1);
            taskRuns[0].should.be.a('object');
            taskRuns[0].should.have.property('status');
            taskRuns[0].status.should.be.a('string');
            taskRuns[0].status.should.be.eq(taskRunModel_1.TaskRunStatus.success);
            taskRuns[0].should.have.property('result');
            taskRuns[0].result.should.be.a('object');
            taskRuns[0].result.should.have.property('result');
            taskRuns[0].result.result.should.be.a('string');
            taskRuns[0].result.result.should.be.eq('task was run');
        });
    });
    it('it should fail to run task', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const existingTask = yield taskModel_1.default.findOne({ name: task.name });
            const res = yield chai_1.default.request(app_1.default.app)
                .post('/tasks/' + existingTask._id + '/run')
                .send({ createError: true });
            res.should.have.status(400);
            const taskRuns = yield taskRunModel_1.default.find({ task: existingTask._id });
            taskRuns.should.be.a('array');
            taskRuns.length.should.be.eq(1);
            taskRuns[0].should.be.a('object');
            taskRuns[0].should.have.property('status');
            taskRuns[0].status.should.be.a('string');
            taskRuns[0].status.should.be.eq(taskRunModel_1.TaskRunStatus.failure);
            taskRuns[0].should.have.property('error');
            taskRuns[0].error.should.be.a('string');
            taskRuns[0].error.should.be.eq('Error: i was thrown');
        });
    });
    it('it should get all task runs', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const existingTask = yield taskModel_1.default.findOne({ name: task.name });
            yield chai_1.default.request(app_1.default.app)
                .post('/tasks/' + existingTask._id + '/run')
                .send();
            yield chai_1.default.request(app_1.default.app)
                .post('/tasks/' + existingTask._id + '/run')
                .send({ createError: true });
            const res = yield chai_1.default.request(app_1.default.app)
                .get('/tasks/' + existingTask._id + '/run')
                .send();
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eq(2);
            res.body[0].should.be.a('object');
            res.body[0].should.have.property('status');
            res.body[0].status.should.be.a('string');
            res.body[0].status.should.be.eq(taskRunModel_1.TaskRunStatus.success);
            res.body[1].should.be.a('object');
            res.body[1].should.have.property('status');
            res.body[1].status.should.be.a('string');
            res.body[1].status.should.be.eq(taskRunModel_1.TaskRunStatus.failure);
        });
    });
    it('should rerun after 2 retries', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const existingTask = yield taskModel_1.default.findOne({ name: task.name });
            const taskRun = new taskRunModel_1.default({
                task: existingTask._id
            });
            yield taskRun.save();
            for (let i = 0; i < 3; i++) {
                yield chai_1.default.request(app_1.default.app)
                    .post('/tasks/' + existingTask._id + '/run')
                    .send();
            }
            const taskRuns = yield taskRunModel_1.default.find();
            taskRuns.should.be.a('array');
            taskRuns.length.should.be.eq(2);
            taskRuns[0].should.have.property('skipCount').with.is.a('number');
            taskRuns[0].skipCount.should.be.eq(2);
        });
    });
});
//# sourceMappingURL=taskTest.spec.js.map
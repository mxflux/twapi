"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const testTaskService_1 = require("./lib/testTaskService");
const taskModel_1 = __importDefault(require("../models/taskModel"));
const twDbConnector_1 = __importDefault(require("../twDbConnector"));
const twTaskService_1 = __importDefault(require("../twTaskService"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
const config_1 = __importDefault(require("./lib/config"));
const testSlowTaskService_1 = require("./lib/testSlowTaskService");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
const twDbConnector = new twDbConnector_1.default(config_1.default.MONGO_DB_HOST, config_1.default.MONGO_DB_DB);
describe('TwTaskService', function () {
    this.timeout(5000);
    beforeEach((done) => {
        // clear job list
        twTaskService_1.default.disable();
        taskModel_1.default.deleteMany({}, (_err) => {
            done();
        });
    });
    before(function (done) {
        twDbConnector.connect();
        done();
    });
    after(function (done) {
        // clear job list
        twTaskService_1.default.disable();
        taskModel_1.default.deleteMany({}, () => {
            twDbConnector.disconnect();
            chai_1.default.spy.restore();
            done();
        });
    });
    it('it should register a new tasks', () => __awaiter(this, void 0, void 0, function* () {
        const task = new taskModel_1.default({
            name: 'TestTask',
            active: true,
            service_path: './lib/testTaskService'
        });
        yield twTaskService_1.default.registerTask(task);
        yield twTaskService_1.default.registerTask(task);
        const tasks = yield taskModel_1.default.find();
        tasks.length.should.be.equal(1);
    }));
    it('it should enable all tasks', () => __awaiter(this, void 0, void 0, function* () {
        const task = new taskModel_1.default({
            name: 'TestTask',
            active: true,
            service_path: './tests/lib/testTaskService',
            cron_rule: '* * * * *'
        });
        yield twTaskService_1.default.registerTask(task);
        yield twTaskService_1.default.enable();
        const tasks = yield taskModel_1.default.find();
        tasks.length.should.be.equal(1);
        twTaskService_1.default.jobs.length.should.be.equal(1);
        twTaskService_1.default.jobs[0].status.should.be.equal('scheduled');
    }));
    it('it should disable all tasks', () => __awaiter(this, void 0, void 0, function* () {
        const task = new taskModel_1.default({
            name: 'TestTask',
            active: true,
            service_path: './tests/lib/testTaskService',
            cron_rule: '* * * * *'
        });
        yield twTaskService_1.default.registerTask(task);
        const task2 = new taskModel_1.default({
            name: 'TestTask2',
            active: true,
            service_path: './tests/lib/testTaskService',
            cron_rule: '2 * * * *'
        });
        yield twTaskService_1.default.registerTask(task2);
        yield twTaskService_1.default.enable();
        yield twTaskService_1.default.disable();
        const tasks = yield taskModel_1.default.find();
        tasks.length.should.be.equal(2);
        twTaskService_1.default.jobs.length.should.be.equal(0);
    }));
    it('it should have called the run function of service', () => __awaiter(this, void 0, void 0, function* () {
        chai_1.default.spy.restore();
        const task = new taskModel_1.default({
            name: 'TestTask',
            active: true,
            service_path: './tests/lib/testTaskService',
            cron_rule: '* * * * * *'
        });
        yield twTaskService_1.default.registerTask(task);
        yield twTaskService_1.default.enable();
        yield timeout(1001);
        testTaskService_1.runned.should.be.eql(true);
    }));
    it('it should not run a task if it is already running', () => __awaiter(this, void 0, void 0, function* () {
        chai_1.default.spy.restore();
        const task = new taskModel_1.default({
            name: 'TestSlowTask',
            active: false,
            service_path: './tests/lib/testSlowTaskService',
            cron_rule: '* * * * * *'
        });
        yield twTaskService_1.default.registerTask(task);
        yield twTaskService_1.default.enable();
        const slowTask = yield twTaskService_1.default.getTaskByName(task.name);
        const firstRunPromise = twTaskService_1.default.executeTask(slowTask, {});
        yield timeout(500);
        yield twTaskService_1.default.executeTask(slowTask, {});
        yield firstRunPromise;
        testSlowTaskService_1.testSlowTaskRunnedCounter.should.be.eq(1);
        testTaskService_1.runned.should.be.eql(true);
    }));
});
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
//# sourceMappingURL=twTaskService.spec.js.map
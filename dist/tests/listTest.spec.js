"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const DummyFactory_1 = __importDefault(require("./lib/DummyFactory"));
const app_1 = __importDefault(require("./lib/app"));
const itemModel_1 = __importDefault(require("./lib/itemModel"));
const chai_1 = __importDefault(require("chai"));
const chaiHttp = require("chai-http");
const spies = require("chai-spies");
process.env.NODE_ENV = 'test';
chai_1.default.use(chaiHttp);
chai_1.default.use(spies);
const dummyFactory = new DummyFactory_1.default(itemModel_1.default);
const paging = [{
        limit: 20,
        offset: 20
    }, {
        limit: 20,
        offset: 60
    },
    {
        limit: 5,
        offset: 7
    }];
describe('Abstract Router List', () => {
    before((done) => {
        app_1.default.listen();
        app_1.default.twDbConnector.connect();
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    after((done) => {
        app_1.default.close().then(() => { done(); });
    });
    beforeEach((done) => {
        dummyFactory.generate(107, done);
    });
    afterEach((done) => {
        itemModel_1.default.deleteMany({}, () => {
            done();
        });
    });
    describe('/GET', () => {
        it('it should GET all the items', (done) => {
            chai_1.default.request(app_1.default.app)
                .get('/items')
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(dummyFactory.dummies.length);
                done();
            });
        });
        it('it should get items with specified name', (done) => {
            // make random number of checks ids
            const checkIds = [];
            for (let i = 0; i < dummyFactory.makeNumber(1, 7); i++) {
                const dummyCheckId = dummyFactory.dummies[dummyFactory.makeNumber(0, (dummyFactory.dummies.length - 1))].name;
                checkIds.push(dummyCheckId);
            }
            const q = `?q={"name": {"$in":["${checkIds.join('","')}"]}}`;
            chai_1.default.request(app_1.default.app)
                .get('/items' + q)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(checkIds.length);
                checkIds.forEach(checkId => {
                    res.body.map((item) => item.name).should.include(checkId);
                });
                done();
            });
        });
        it('it should sort items by name asc', (done) => {
            const s = '?s={"name":1}';
            dummyFactory.dummies.sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            });
            chai_1.default.request(app_1.default.app)
                .get('/items' + s)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(dummyFactory.dummies.length);
                res.body.map((item) => item.name).should.be.deep.equal(dummyFactory.dummies.map(item => item.name));
                done();
            });
        });
        it('it should sort items by name desc', (done) => {
            const s = '?s={"name":-1}';
            dummyFactory.dummies.sort((a, b) => {
                if (a.name < b.name) {
                    return 1;
                }
                if (a.name > b.name) {
                    return -1;
                }
                return 0;
            });
            chai_1.default.request(app_1.default.app)
                .get('/items' + s)
                .end((_err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(dummyFactory.dummies.length);
                res.body.map((item) => item.name).should.be.deep.equal(dummyFactory.dummies.map(item => item.name));
                done();
            });
        });
        paging.forEach((con) => {
            const limit = con.limit;
            const offset = con.offset;
            it(`it should GET ${limit} surveys `, (done) => {
                const p = `?p={"limit":${limit},"offset":0}`;
                chai_1.default.request(app_1.default.app)
                    .get('/items' + p)
                    .end((_err, res) => {
                    res.should.have.status(200);
                    res.header.should.have.property('link');
                    res.get('x-total-count').should.be.equal(dummyFactory.dummies.length.toString());
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(limit);
                    res.body[0]._id.should.be.eql(dummyFactory.dummies[0]._id.toString());
                    res.body[limit - 1]._id.should.be.eql(dummyFactory.dummies[limit - 1]._id.toString());
                    done();
                });
            });
            it(`it should GET ${limit} surveys with offset of ${offset}`, (done) => {
                const p = `?p={"limit":${limit},"offset":${offset}}`;
                const lastOffset = Math.floor(dummyFactory.dummies.length / limit) * limit;
                const lastLimit = (dummyFactory.dummies.length - lastOffset);
                const nextLimit = (lastOffset === offset + 1 * limit) ? lastLimit : limit;
                const expectedLink = `</items?&p={"limit":${nextLimit},"offset":${limit + offset}>; rel="next", </items?&p={"limit":${limit},"offset":${offset - limit}>; rel="prev", </items?&p={"limit":${limit},"offset":0>; rel="first", </items?&p={"limit":${lastLimit},"offset":${lastOffset}>; rel="last"`;
                chai_1.default.request(app_1.default.app)
                    .get('/items' + p)
                    .end((_err, res) => {
                    res.should.have.status(200);
                    res.header.should.have.property('link');
                    res.header.link.should.be.equal(expectedLink);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(limit);
                    res.body[0]._id.should.be.eql(dummyFactory.dummies[offset]._id.toString());
                    res.body[limit - 1]._id.should.be.eql(dummyFactory.dummies[offset + limit - 1]._id.toString());
                    done();
                });
            });
        });
        it('it should GET projected subitem only', (done) => {
            // add subitems to first item
            dummyFactory.dummies[0].subItems.push(new itemModel_1.default({ name: 'first', number: 1 }));
            dummyFactory.dummies[0].subItems.push(new itemModel_1.default({ name: 'second', number: 2 }));
            dummyFactory.dummies[0].subItems.push(new itemModel_1.default({ name: 'also first', number: 1 }));
            dummyFactory.dummies[0].save().then(() => {
                const o = '&o={"subItems.$":1}';
                const q = `?q={"_id":"${dummyFactory.dummies[0]._id}","subItems.number":1}`;
                chai_1.default.request(app_1.default.app)
                    .get('/items' + q + o)
                    .end((_err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    res.body[0].should.have.property('subItems');
                    res.body[0].subItems.length.should.be.eql(1);
                    res.body[0].subItems[0].name.should.be.eql('first');
                    done();
                });
            });
        });
    });
});
//# sourceMappingURL=listTest.spec.js.map
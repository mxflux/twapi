declare class TwDbConnector {
    private host;
    private database;
    private connected;
    constructor(host: string, database: string);
    connect(): void;
    disconnect(): Promise<void>;
}
export default TwDbConnector;

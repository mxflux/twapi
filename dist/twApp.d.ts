/// <reference types="node" />
import { Application } from 'express';
import TwDbConnector from './twDbConnector';
import { Server } from 'http';
declare class TwApp {
    port: number;
    isInLambda: boolean;
    app: Application | any;
    twDbConnector: TwDbConnector;
    private twWatchService;
    private server;
    constructor(port?: number, isInLambda?: boolean);
    connectDB(host: string, database: string): void;
    enableWatchSerive(watchServiceConfig: any): void;
    registerTask(task: any): void;
    enableTasks(): void;
    addRoutes(routes: any): void;
    setPort(port: number): void;
    listen(): any;
    listenLambda(): (event: any, context: any) => Promise<void> | Server;
    executeTask(event: any): Promise<void>;
    close(): Promise<void>;
}
export default TwApp;

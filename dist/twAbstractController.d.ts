import { Request, Response } from 'express';
import { Document, Model } from 'mongoose';
import { Indexable } from './types/basicTypes';
interface POPOBJ {
    populate: any[];
}
interface Config {
    list?: POPOBJ;
    get?: POPOBJ;
}
declare class TwAbstractController {
    protected config: Config;
    protected model: Model<Document>;
    constructor(model?: Model<any> | null);
    /**
     * expects no parameters but a query eg:
     * http://localhost:3000/surveys?q={"zahl": {"$gt":1}}&s={"zahl":1}&p={"limit":2,"offset":1}&o={"subItems.$":1}
     * q = Query
     * s = Sorting
     * p = Paging (limit, offset)
     * o = Projection
     * @param req
     * @param res
     */
    list(req: Request, res: Response): Promise<any>;
    createList(res: Response, req: Request, queryFields: any): void;
    getQuery(req: Request): {
        query: Indexable;
        sorting: Indexable;
        limit: number;
        offset: number;
        projection: any;
        populate: {
            path: string;
            match: Indexable;
        }[];
    };
    get(req: Request, res: Response): Promise<void>;
    post(req: Request, res: Response): Promise<void>;
    put(req: Request, res: Response): Promise<void>;
    patch(req: Request, res: Response): Promise<void>;
    delete(req: Request, res: Response): Promise<void>;
    createPagingLinks(req: Request, res: Response, limit: number, offset: number, count: number): void;
    saveDocument(document: Document): Promise<unknown>;
    getItem(id: string): any;
    saveModel(model: any): Promise<unknown>;
}
export default TwAbstractController;
